<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c"%>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleZncClaimForm"/>

<c:set var="namespace" value="<%= renderResponse.getNamespace() %>" />
<script type="text/javascript">
 $(function() {
	var phoneProducerMap = {};
	<c:forEach var="phonesByProducer" items="${phoneModelsMap}">
		var phoneProducerKey = "${phonesByProducer.key}";
		var phones = [];
		<c:forEach var="phoneModel" items="${phoneModelsMap[phonesByProducer.key]}">
			var phoneModel = "${phoneModel}";
			phones.push(phoneModel);
		</c:forEach>

		phoneProducerMap[phoneProducerKey] = phones;
	</c:forEach>
	console.log(phoneProducerMap);

	var phoneProducerSelect = $("#${namespace}phoneProducer");
	var modelSelect = $("#${namespace}phoneModel");
	phoneProducerSelect.change(function() {
		modelSelect.empty();
		var currentProducer = phoneProducerSelect.val();
		var models = phoneProducerMap[currentProducer];
		if (models && (models.length > 0)) {
			for (var i = 0; i < models.length; i++) {
				modelSelect.append("<option value='" + models[i] + "'>" + models[i] + "</option>");
			}
			modelSelect.prop("disabled", false);
		} else {
			modelSelect.prop("disabled", true);
		}
	});
});
</script>

<div class="page-width page-width--edge page-indent h-text-center">
	<div class="just-box just-box--inline">
		<form:form name="zncClaimDto" method="post" modelAttribute="zncClaimDto" action="<%=submitFormURL.toString() %>">
			<div>
				<form:errors path="firstName" />
				<div class="contact-us-box__item__text-input"><form:input path="firstName" placeholder="Jméno"/></div>
			</div>
			<div>
				<form:errors path="lastName" />
				<form:input path="lastName" placeholder="Příjmení" />
			</div>
			<div>
				<form:errors path="customerPhone" />
				<form:input path="customerPhone" placeholder="Telefonní číslo" />
			</div>
			<div>
				<form:errors path="customerEmail" />
				<form:input path="customerEmail" placeholder="Email" />
			</div>
			<div>
				<form:errors path="price" />
				<form:input path="price" placeholder="Cena zakoupeného telefonu" />
			</div>
			<div>
				<form:errors path="phoneProducer" />
				<form:select path="phoneProducer" id="${namespace}phoneProducer">
					<form:option value="" label="Výrobce" selected="true" disabled="true"/>
					<form:options items="${phoneProducers}" />
				</form:select>
			</div>
			<div>
				<form:errors path="phoneModel" />
				<form:select path="phoneModel" id="${namespace}phoneModel">
					<form:option value="" label="Typ telefonu" selected="true" disabled="true"/>
				</form:select>
			</div>
			<div>
				<form:errors path="shop" />
				<form:input path="shop" placeholder="Místo zakoupení telefonu"/>
			</div>
			<div>
				<form:errors path="phoneImei" />
				<form:input path="phoneImei" placeholder="IMEI číslo telefonu"/>
			</div>
			<div>
				<form:errors path="tariff" />
				<form:select path="tariff" id="${namespace}tariff">
					<form:option value="" label="Tarif" selected="true" disabled="true"/>
					<form:options items="${tariffs}" />
				</form:select>
			</div>
			<div>
				<form:errors path="competitor" />
				<form:select path="competitor" id="${namespace}competitor">
					<form:option value="" label="Obchod s lepší cenou" selected="true" disabled="true"/>
					<form:options items="${competitors}" />
				</form:select>
			</div>
			<div>
				<form:errors path="dateString" />
				<form:input path="dateString" type="date" placeholder="Datum nákupu telefonu" />
			</div>
			<button type="submit" class="button button--blue">Odeslat</button>
		</form:form>
	</div>
</div>
