package cz.artin.mvp.znc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.artin.mvp.znc.model.ZncClaim;

/**
 * Represents DAO for claims.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Repository
public class ZncClaimDaoHibernate implements ZncClaimDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public long create(ZncClaim claim) {
        return (Long) sessionFactory.getCurrentSession().save(claim);
    }

    @Override
    public List<ZncClaim> findClaimsToSendO2(int claimsToSendAtOnce, int tryLimit) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(ZncClaim.class);

        crit.setMaxResults(claimsToSendAtOnce);

        crit.addOrder(Order.desc("date"));
        if (tryLimit > 0) {
            crit.add(Restrictions.le("o2Tries", tryLimit));
        }
        crit.add(Restrictions.isNull("o2MailSentDate"));

        @SuppressWarnings("unchecked")
        List<ZncClaim> result = crit.list();
        return result;
    }

    @Override
    public List<ZncClaim> findClaimsToSendCustomers(int claimsToSendAtOnce, int tryLimit) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(ZncClaim.class);

        crit.setMaxResults(claimsToSendAtOnce);

        crit.addOrder(Order.desc("date"));
        if (tryLimit > 0) {
            crit.add(Restrictions.le("customerTries", tryLimit));
        }
        crit.add(Restrictions.isNull("customerMailSentDate"));

        @SuppressWarnings("unchecked")
        List<ZncClaim> result = crit.list();
        return result;
    }

    @Override
    public List<ZncClaim> findClaimsByImei(String imei) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(ZncClaim.class);

        crit.add(Restrictions.eq("phoneImei", imei));
        @SuppressWarnings("unchecked")
        List<ZncClaim> result = crit.list();
        return result;
    }

}
