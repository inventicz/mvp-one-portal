package cz.artin.mvp.znc.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.annotation.PostConstruct;
import javax.portlet.PortletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portlet.dynamicdatalists.model.DDLRecord;
import com.liferay.portlet.dynamicdatalists.model.DDLRecordSet;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordSetLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.storage.Field;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;

import cz.artin.mvp.znc.model.ZncConfiguration;
import cz.artin.mvp.znc.util.LiferayDdlUtil;

/**
 * Represents configuration related service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {
    private Log log = LogFactoryUtil.getLog(ConfigurationServiceImpl.class);

    @Value("${ddl.configuration.recordset.name}")
    private String configurationRecordSetName;

    @Value("${ddl.phones.recordset.name}")
    private String phonesRecordSetName;

    @Value("${ddl.tariffs.recordset.name}")
    private String tariffsRecordSetName;

    @Value("${ddl.competitors.recordset.name}")
    private String competitorsRecordSetName;

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${ddl.configuration.xsd.path}'))}")
    private String configurationDdlXsd;

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${ddl.phones.xsd.path}'))}")
    private String phonesDdlXsd;

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${ddl.tariffs.xsd.path}'))}")
    private String tariffsDdlXsd;

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${ddl.competitors.xsd.path}'))}")
    private String competitorsDdlXsd;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.mail_from.default}")
    private String defaultEmailAddressFrom;
    @Value("${ddl.mail_from.key}")
    private String mailFromKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.mail_to.default}")
    private String defaultEmailAddressTo;
    @Value("${ddl.mail_to.key}")
    private String mailToKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.first_name_error_text.default}")
    private String defaultFirstNameErrorText;
    @Value("${ddl.first_name_error_text.key}")
    private String firstNameErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.last_name_error_text.default}")
    private String defaultLastNameErrorText;
    @Value("${ddl.last_name_error_text.key}")
    private String lastNameErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.phone_error_text.default}")
    private String defaultPhoneErrorText;
    @Value("${ddl.phone_error_text.key}")
    private String phoneErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.mail_error_text.default}")
    private String defaultMailErrorText;
    @Value("${ddl.mail_error_text.key}")
    private String mailErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.competitor_error_text.default}")
    private String defaultCompetitorErrorText;
    @Value("${ddl.competitor_error_text.key}")
    private String competitorErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.phone_producer_error_text.default}")
    private String defaultPhoneProducerErrorText;
    @Value("${ddl.phone_producer_error_text.key}")
    private String phoneProducerErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.imei_duplicity_error_text.default}")
    private String defaultImeiDuplicityErrorText;
    @Value("${ddl.imei_duplicity_error_text.key}")
    private String imeiDuplicityErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.imei_error_text.default}")
    private String defaultImeiErrorText;
    @Value("${ddl.imei_error_text.key}")
    private String imeiErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.date_error_text.default}")
    private String defaultDateErrorText;
    @Value("${ddl.date_error_text.key}")
    private String dateErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.phone_model_error_text.default}")
    private String defaultPhoneModelErrorText;
    @Value("${ddl.phone_model_error_text.key}")
    private String phoneModelErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.price_error_text.default}")
    private String defaultPriceErrorText;
    @Value("${ddl.price_error_text.key}")
    private String priceErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.shop_error_text.default}")
    private String defaultShopErrorText;
    @Value("${ddl.shop_error_text.key}")
    private String shopErrorTextKey;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.tariff_error_text.default}")
    private String defaultTariffErrorText;
    @Value("${ddl.tariff_error_text.key}")
    private String tariffErrorTextKey;

    private ZncConfiguration configuration;

    @PostConstruct
    public void init() {

    }

    @Override
    public ZncConfiguration initConfiguration(PortletRequest request) {
        configuration = new ZncConfiguration();

        readConfigurationDdl(request);

        readPhonesDdl(request);

        readTariffsDdl(request);

        readCompetitorsDdl(request);

        return this.configuration;
    }

    @Override
    public ZncConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public ZncConfiguration getConfiguration(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("getConfiguration - request=" + request);
        }
        if (configuration == null) {
            if (log.isDebugEnabled()) {
                log.debug("getConfiguration - configuration not initialized");
            }
            readConfigurationDdl(request);
        }
        return this.configuration;
    }

    private void readConfigurationDdl(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - request=" + request);
        }
        DDLRecordSet configRecordSet = this.getDdlRecordSetByName(configurationRecordSetName);
        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - configRecordSet=" + configRecordSet);
        }
        if (configRecordSet == null) {
            configRecordSet = createConfigurationRecordset(request);
            if (log.isDebugEnabled()) {
                log.debug("readConfigurationDdl - created configRecordSet=" + configRecordSet);
            }

            storeDefaultValuesToDdlRecord(configRecordSet, request);
        }

        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - going to read configuration");
        }
        readConfiguration(configRecordSet, request);
    }

    private void readPhonesDdl(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readPhonesDdl - request=" + request);
        }
        DDLRecordSet phonesRecordSet = this.getDdlRecordSetByName(phonesRecordSetName);
        if (log.isDebugEnabled()) {
            log.debug("readPhonesDdl - phonesRecordSet=" + phonesRecordSet);
        }
        if (phonesRecordSet == null) {
            phonesRecordSet = createPhonesRecordset(request);
            if (log.isDebugEnabled()) {
                log.debug("readPhonesDdl - created phonesRecordSet=" + phonesRecordSet);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("readPhonesDdl - going to read phones");
        }
        readPhones(phonesRecordSet, request);
    }

    private void readTariffsDdl(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readTariffsDdl - request=" + request);
        }
        DDLRecordSet tariffsRecordSet = this.getDdlRecordSetByName(tariffsRecordSetName);
        if (log.isDebugEnabled()) {
            log.debug("readTariffsDdl - tariffsRecordSet=" + tariffsRecordSet);
        }
        if (tariffsRecordSet == null) {
            tariffsRecordSet = createTariffsRecordset(request);
            if (log.isDebugEnabled()) {
                log.debug("readTariffsDdl - created tariffsRecordSet=" + tariffsRecordSet);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("readTariffsDdl - going to read tariffs");
        }
        readTariffs(tariffsRecordSet, request);
    }

    private void readCompetitorsDdl(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readCompetitorsDdl - request=" + request);
        }
        DDLRecordSet competitorsRecordSet = this.getDdlRecordSetByName(competitorsRecordSetName);
        if (log.isDebugEnabled()) {
            log.debug("readCompetitorsDdl - competitorsRecordSet=" + competitorsRecordSet);
        }
        if (competitorsRecordSet == null) {
            competitorsRecordSet = createCompetitorsRecordset(request);
            if (log.isDebugEnabled()) {
                log.debug("readCompetitorsDdl - created competitorsRecordSet=" + competitorsRecordSet);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("readCompetitorsDdl - going to read competitors");
        }
        readCompetitors(competitorsRecordSet, request);
    }

    /**
     * Read DDLRecordSet.
     *
     * @return DDLRecordSet with the name or null if record was not found
     */
    private DDLRecordSet getDdlRecordSetByName(String name) {
        DDLRecordSet result = null;

        List<DDLRecordSet> ddlRecordSets;
        try {
            ddlRecordSets = DDLRecordSetLocalServiceUtil.getDDLRecordSets(0,
                    DDLRecordSetLocalServiceUtil.getDDLRecordSetsCount());
            if (ddlRecordSets != null) {
                for (DDLRecordSet record : ddlRecordSets) {
                    if (record.getNameCurrentValue().equalsIgnoreCase(name)) {
                        result = record;
                        break;
                    }
                }
            }
        } catch (SystemException e) {
            log.warn("DDLRecordSet with name " + name + "was not found.");
        }

        return result;
    }

    private DDLRecordSet createConfigurationRecordset(PortletRequest request) {
        DDMStructure structure = LiferayDdlUtil.createDdmStructureIfNotExists(configurationRecordSetName,
                configurationDdlXsd, request);

        return LiferayDdlUtil.createDdlRecordSet(configurationRecordSetName, structure, request);
    }

    private DDLRecordSet createPhonesRecordset(PortletRequest request) {
        DDMStructure structure = LiferayDdlUtil.createDdmStructureIfNotExists(phonesRecordSetName, phonesDdlXsd,
                request);

        return LiferayDdlUtil.createDdlRecordSet(phonesRecordSetName, structure, request);
    }

    private DDLRecordSet createTariffsRecordset(PortletRequest request) {
        DDMStructure structure = LiferayDdlUtil.createDdmStructureIfNotExists(tariffsRecordSetName, tariffsDdlXsd,
                request);

        return LiferayDdlUtil.createDdlRecordSet(tariffsRecordSetName, structure, request);
    }

    private DDLRecordSet createCompetitorsRecordset(PortletRequest request) {
        DDMStructure structure = LiferayDdlUtil.createDdmStructureIfNotExists(competitorsRecordSetName,
                competitorsDdlXsd, request);

        return LiferayDdlUtil.createDdlRecordSet(competitorsRecordSetName, structure, request);
    }

    private void storeDefaultValuesToDdlRecord(DDLRecordSet configRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("storeDefaultValuesToDdlRecord - configRecordSet=" + configRecordSet);
        }

        Fields fields = new Fields();

        fields.put(new Field(mailFromKey, defaultEmailAddressFrom));
        fields.put(new Field(mailToKey, defaultEmailAddressTo));
        fields.put(new Field(firstNameErrorTextKey, defaultFirstNameErrorText));
        fields.put(new Field(lastNameErrorTextKey, defaultLastNameErrorText));
        fields.put(new Field(phoneErrorTextKey, defaultPhoneErrorText));
        fields.put(new Field(mailErrorTextKey, defaultMailErrorText));
        fields.put(new Field(competitorErrorTextKey, defaultCompetitorErrorText));
        fields.put(new Field(phoneProducerErrorTextKey, defaultPhoneProducerErrorText));
        fields.put(new Field(imeiDuplicityErrorTextKey, defaultImeiDuplicityErrorText));
        fields.put(new Field(imeiErrorTextKey, defaultImeiErrorText));
        fields.put(new Field(dateErrorTextKey, defaultDateErrorText));
        fields.put(new Field(phoneModelErrorTextKey, defaultPhoneModelErrorText));
        fields.put(new Field(priceErrorTextKey, defaultPriceErrorText));
        fields.put(new Field(shopErrorTextKey, defaultShopErrorText));
        fields.put(new Field(tariffErrorTextKey, defaultTariffErrorText));

        LiferayDdlUtil.createDdlRecord(fields, configRecordSet, request);
        if (log.isDebugEnabled()) {
            log.debug("storeDefaultValuesToDdlRecord - DdlRecord created");
        }
    }

    private ZncConfiguration readConfiguration(DDLRecordSet configRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readConfiguration - configRecordSet=" + configRecordSet);
        }

        if (configRecordSet == null) {
            log.warn("DDL configuration does not exists.");
        } else {
            DDLRecord configurationRecord = null;
            try {
                List<DDLRecord> records = configRecordSet.getRecords();
                int recordsCount = records.size();
                if (recordsCount == 1) {
                    log.debug("DDL configuration record found.");
                    configurationRecord = records.get(0);
                    if (log.isDebugEnabled()) {
                        log.debug("readConfiguration - configRecordSet=" + configRecordSet);
                        log.debug("readConfiguration - configRecord=" + configurationRecord);
                    }
                } else if (recordsCount == 0) {
                    log.warn("No DDL configuration record found.");

                    storeDefaultValuesToDdlRecord(configRecordSet, request);
                } else {
                    log.warn("More DDL configuration record found. Using first one.");
                    configurationRecord = records.get(0);
                }

            } catch (SystemException e) {
                log.warn("Problem reading DDL configuration.", e);
            }

            if (configurationRecord != null) {
                try {
                    Fields fields = configurationRecord.getFields();
                    configuration.setMailFrom(readFieldValue(fields, mailFromKey, defaultEmailAddressFrom));
                    configuration.setMailTo(readFieldValue(fields, mailToKey, defaultEmailAddressTo));
                    configuration.setFirstNameErrorText(
                            readFieldValue(fields, firstNameErrorTextKey, defaultFirstNameErrorText));
                    configuration.setLastNameErrorText(
                            readFieldValue(fields, lastNameErrorTextKey, defaultLastNameErrorText));
                    configuration.setPhoneErrorText(readFieldValue(fields, phoneErrorTextKey, defaultPhoneErrorText));
                    configuration.setMailErrorText(readFieldValue(fields, mailErrorTextKey, defaultMailErrorText));
                    configuration.setCompetitorErrorText(
                            readFieldValue(fields, competitorErrorTextKey, defaultCompetitorErrorText));
                    configuration.setPhoneProducerErrorText(
                            readFieldValue(fields, phoneProducerErrorTextKey, defaultPhoneProducerErrorText));
                    configuration.setImeiDuplicityErrorText(
                            readFieldValue(fields, imeiDuplicityErrorTextKey, defaultImeiDuplicityErrorText));
                    configuration.setImeiErrorText(readFieldValue(fields, imeiErrorTextKey, defaultImeiErrorText));
                    configuration.setDateErrorText(readFieldValue(fields, dateErrorTextKey, defaultDateErrorText));
                    configuration.setPhoneModelErrorText(
                            readFieldValue(fields, phoneModelErrorTextKey, defaultPhoneModelErrorText));
                    configuration.setPriceErrorText(readFieldValue(fields, priceErrorTextKey, defaultPriceErrorText));
                    configuration.setShopErrorText(readFieldValue(fields, shopErrorTextKey, defaultShopErrorText));
                    configuration
                            .setTariffErrorText(readFieldValue(fields, tariffErrorTextKey, defaultTariffErrorText));
                } catch (PortalException e) {
                    log.warn("Problem reading DDL configuration.", e);
                }
            }
        }

        return configuration;
    }

    private void readPhones(DDLRecordSet phonesRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readPhones - phonesRecordSet=" + phonesRecordSet);
        }

        Map<String, Set<String>> phoneProducerMap = new TreeMap<>();

        if (phonesRecordSet == null) {
            log.warn("DDL phones does not exists.");
        } else {
            try {
                List<DDLRecord> records = phonesRecordSet.getRecords();
                for (DDLRecord ddlPhoneRecord : records) {
                    Fields fields = ddlPhoneRecord.getFields();
                    String producer = readFieldValue(fields, "producer");
                    String phone = readFieldValue(fields, "phone");

                    if (!StringUtils.hasText(producer)) {
                        log.warn("Producer is not set correctly - " + ddlPhoneRecord);
                        continue;
                    }

                    if (!StringUtils.hasText(phone)) {
                        log.warn("Phone is not set correctly - " + ddlPhoneRecord);
                        continue;
                    }

                    Set<String> producerPhones = phoneProducerMap.get(producer);
                    if (producerPhones == null) {
                        producerPhones = new TreeSet<>();
                        phoneProducerMap.put(producer, producerPhones);
                    }
                    producerPhones.add(phone);
                }
                configuration.setPhones(phoneProducerMap);
            } catch (SystemException | PortalException e) {
                log.warn("Problem reading DDL phones.", e);
            }
        }
    }

    private void readTariffs(DDLRecordSet tariffsRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readTariffs - tariffsRecordSet=" + tariffsRecordSet);
        }

        List<String> tariffs = new ArrayList<>();

        if (tariffsRecordSet == null) {
            log.warn("DDL tariffs does not exists.");
        } else {
            try {
                List<DDLRecord> records = tariffsRecordSet.getRecords();
                for (DDLRecord ddlTariffsRecord : records) {
                    Fields fields = ddlTariffsRecord.getFields();
                    String tariff = readFieldValue(fields, "tariff");

                    if (!StringUtils.hasText(tariff)) {
                        log.warn("Tariff is not set correctly - " + ddlTariffsRecord);
                        continue;
                    }

                    tariffs.add(tariff);
                }
                configuration.setTariffs(tariffs);
            } catch (SystemException | PortalException e) {
                log.warn("Problem reading DDL tariffs.", e);
            }

        }
    }

    private void readCompetitors(DDLRecordSet competitorsRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readCompetitors - competitorsRecordSet=" + competitorsRecordSet);
        }

        List<String> competitors = new ArrayList<>();

        if (competitorsRecordSet == null) {
            log.warn("DDL competitors does not exists.");
        } else {
            try {
                List<DDLRecord> records = competitorsRecordSet.getRecords();
                for (DDLRecord ddlCompetitorsRecord : records) {
                    Fields fields = ddlCompetitorsRecord.getFields();
                    String competitor = readFieldValue(fields, "competitor");

                    if (!StringUtils.hasText(competitor)) {
                        log.warn("Competitor is not set correctly - " + ddlCompetitorsRecord);
                        continue;
                    }

                    competitors.add(competitor);
                }
                configuration.setCompetitors(competitors);
            } catch (SystemException | PortalException e) {
                log.warn("Problem reading DDL competitors.", e);
            }

        }
    }

    private String readFieldValue(Fields fields, String fieldKey) {
        String value = readFieldValue(fields, fieldKey, null);
        final String result;
        if ("producer".equals(fieldKey) && value.startsWith("[\"")) {
            result = value.substring(2, value.length() - 2);
        } else {
            result = value;
        }

        return result;
    }

    private String readFieldValue(Fields fields, String fieldKey, String defaultValue) {
        final String result;

        Field field = fields.get(fieldKey);
        if (field != null) {
            Serializable value = field.getValue();
            if (value != null) {
                result = value.toString();
            } else {
                log.warn("Configuration value for: " + fieldKey + " was not found. Using default value.");
                result = defaultValue;
            }
        } else {
            log.warn("Configuration field: " + fieldKey + " was not found. Using default value.");
            result = defaultValue;
        }

        return result;
    }

}
