package cz.artin.mvp.znc.service;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cz.artin.mvp.znc.dao.ZncClaimDao;
import cz.artin.mvp.znc.model.ZncClaim;
import cz.artin.mvp.znc.model.ZncClaimDto;
import cz.artin.mvp.znc.model.ZncConfiguration;

@Service
public class ZncServiceImpl implements ZncService {

    /** How many times will try server to send mail. */
    @Value("${email.o2.queue.max_try}")
    private int o2TryLimit;

    /** How many mail will be send in one cycle. */
    @Value("${email.o2.queue.at_once}")
    private int o2ClaimsToSendAtOnce;

    /** How many times will try server to send mail. */
    @Value("${email.customer.queue.max_try}")
    private int customerTryLimit;

    /** How many mail will be send in one cycle. */
    @Value("${email.customer.queue.at_once}")
    private int customerClaimsToSendAtOnce;

    @Autowired
    private ZncClaimDao dao;

    @Override
    @Transactional
    public ZncClaim createZncClaim(ZncClaimDto zncClaimDto, ZncConfiguration configuration, String url) {
        ZncClaim claim = new ZncClaim();
        claim.setPhoneProducer(StringUtils.left(zncClaimDto.getPhoneProducer(), 31));
        claim.setPhoneImei(StringUtils.left(zncClaimDto.getPhoneImei(), 31));
        claim.setPhoneModel(StringUtils.left(zncClaimDto.getPhoneModel(), 63));
        claim.setCompetitor(StringUtils.left(zncClaimDto.getCompetitor(), 255));
        claim.setFirstName(StringUtils.left(zncClaimDto.getFirstName(), 255));
        claim.setLastName(StringUtils.left(zncClaimDto.getLastName(), 255));
        claim.setCustomerPhone(StringUtils.left(zncClaimDto.getCustomerPhone(), 255));
        claim.setCustomerEmail(StringUtils.left(zncClaimDto.getCustomerEmail(), 255));
        claim.setTariff(StringUtils.left(zncClaimDto.getTariff(), 255));
        claim.setPrice(zncClaimDto.getPrice());
        claim.setShop(StringUtils.left(zncClaimDto.getShop(), 255));
        claim.setBuyDate(zncClaimDto.getDate());
        claim.setDate(new Date());
        claim.setUrl(StringUtils.left(url, 255));

        dao.create(claim);

        return claim;
    }

    @Override
    @Transactional
    public boolean existsClaimWithImei(String imei) {
        List<ZncClaim> claims = dao.findClaimsByImei(imei);
        return !claims.isEmpty();
    }

    @Override
    public List<ZncClaim> findO2EmailToSend() {
        return dao.findClaimsToSendO2(o2ClaimsToSendAtOnce, o2TryLimit);
    }

    @Override
    public List<ZncClaim> findCustomerEmailToSend() {
        return dao.findClaimsToSendCustomers(customerClaimsToSendAtOnce, customerTryLimit);
    }

}
