package cz.artin.mvp.znc.service;

import java.util.List;

import cz.artin.mvp.znc.model.ZncClaim;
import cz.artin.mvp.znc.model.ZncClaimDto;
import cz.artin.mvp.znc.model.ZncConfiguration;

/**
 * Business layer for ZNC portlet
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface ZncService {
    /**
     * Create new {@link ZncClaim}.
     *
     * @param zncClaimDto
     *            DTO from presentation layer
     * @param configuration
     *            configuration of portlet
     * @param url
     *            source url of page with portlet
     * @return created {@link ZncClaim}
     */
    ZncClaim createZncClaim(ZncClaimDto zncClaimDto, ZncConfiguration configuration, String url);

    boolean existsClaimWithImei(String imei);

    List<ZncClaim> findO2EmailToSend();

    List<ZncClaim> findCustomerEmailToSend();
}
