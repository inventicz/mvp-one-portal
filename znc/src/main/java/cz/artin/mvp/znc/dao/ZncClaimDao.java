package cz.artin.mvp.znc.dao;

import java.util.List;

import cz.artin.mvp.znc.model.ZncClaim;

/**
 * Dao for main project class.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface ZncClaimDao {
    /**
     * Create new {@link ZncClaim}.
     */
    long create(ZncClaim claim);

    /**
     * Find claims by imei.
     */
    List<ZncClaim> findClaimsByImei(String imei);

    /**
     * Read claims for mailing to O2.
     *
     * @param claimsToSendAtOnce
     *            number of claims (oldest first) to read, if 0 no limit will be
     *            applied
     * @param tryLimit
     *            how many times will try server to send mail, if 0 no limit
     *            will be applied
     */
    List<ZncClaim> findClaimsToSendO2(int claimsToSendAtOnce, int tryLimit);

    /**
     * Read claims for mailing to customers.
     *
     * @param claimsToSendAtOnce
     *            number of claims (oldest first) to read, if 0 no limit will be
     *            applied
     * @param tryLimit
     *            how many times will try server to send mail, if 0 no limit
     *            will be applied
     */
    List<ZncClaim> findClaimsToSendCustomers(int claimsToSendAtOnce, int tryLimit);
}
