package cz.artin.mvp.znc.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Index;

/**
 * Represents ZNC Claim from web.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Entity
@Table(schema = "OP_ZNC_USER", name = "znc_data")
public class ZncClaim implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column(nullable = false, name = "datum")
    @Index(name = "ori_data_znc_date_ix")
    private Date date;

    @Column(nullable = false, name = "buy_date")
    private Date buyDate;

    @Column(nullable = false, length = 255, name = "url")
    @Index(name = "ori_data_znc_url_ix")
    private String url;

    @Column(length = 31, nullable = false, name = "phone_producer")
    private String phoneProducer;

    @Column(length = 31, nullable = false, name = "phone_imei")
    @Index(name = "ori_data_znc_imei_ix")
    private String phoneImei;

    @Column(length = 63, nullable = false, name = "phone_model")
    private String phoneModel;

    @Column(nullable = false, length = 255, name = "shop")
    private String shop;

    @Column(nullable = false, length = 255, name = "first_name")
    private String firstName;

    @Column(nullable = false, length = 255, name = "last_name")
    private String lastName;

    @Column(nullable = false, length = 255, name = "customer_phone")
    private String customerPhone;

    @Column(nullable = false, length = 255, name = "customer_email")
    private String customerEmail;

    @Column(name = "customer_mail_sent_date")
    private Date customerMailSentDate;

    @Column(name = "customer_mail_last_try_date")
    private Date customerMailLastTryDate;

    @Column(name = "customer_tries", nullable = false)
    private Integer customerTries = 0;

    @Column(name = "o2_mail_sent_date")
    private Date o2MailSentDate;

    @Column(name = "o2_mail_last_try_date")
    private Date o2MailLastTryDate;

    @Column(name = "o2_tries", nullable = false)
    private Integer o2Tries = 0;

    @Column(nullable = false, length = 255, name = "competitor")
    private String competitor;

    @Column(nullable = false, length = 255, name = "tariff")
    private String tariff;

    @Column(nullable = false, name = "price")
    private BigDecimal price;

    // ----
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPhoneProducer() {
        return phoneProducer;
    }

    public void setPhoneProducer(String phoneProducer) {
        this.phoneProducer = phoneProducer;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getShop() {
        return shop;
    }

    public void setShop(String shop) {
        this.shop = shop;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public Date getCustomerMailSentDate() {
        return customerMailSentDate;
    }

    public void setCustomerMailSentDate(Date customerMailSentDate) {
        this.customerMailSentDate = customerMailSentDate;
    }

    public Date getCustomerMailLastTryDate() {
        return customerMailLastTryDate;
    }

    public void setCustomerMailLastTryDate(Date customerMailLastTryDate) {
        this.customerMailLastTryDate = customerMailLastTryDate;
    }

    public Integer getCustomerTries() {
        return customerTries;
    }

    public void setCustomerTries(Integer customerTries) {
        this.customerTries = customerTries;
    }

    public Date getO2MailSentDate() {
        return o2MailSentDate;
    }

    public void setO2MailSentDate(Date o2MailSentDate) {
        this.o2MailSentDate = o2MailSentDate;
    }

    public Date getO2MailLastTryDate() {
        return o2MailLastTryDate;
    }

    public void setO2MailLastTryDate(Date o2MailLastTryDate) {
        this.o2MailLastTryDate = o2MailLastTryDate;
    }

    public Integer getO2Tries() {
        return o2Tries;
    }

    public void setO2Tries(Integer o2Tries) {
        this.o2Tries = o2Tries;
    }

    public String getPhoneImei() {
        return phoneImei;
    }

    public void setPhoneImei(String phoneImei) {
        this.phoneImei = phoneImei;
    }

    public String getCompetitor() {
        return competitor;
    }

    public void setCompetitor(String competitor) {
        this.competitor = competitor;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Date getBuyDate() {
        return buyDate;
    }

    public void setBuyDate(Date buyDate) {
        this.buyDate = buyDate;
    }

    // ----
    @Override
    public String toString() {
        return new ToStringBuilder(this).append(this.id).append(this.date).append(this.customerEmail).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        ZncClaim other = (ZncClaim) obj;

        return new EqualsBuilder().append(this.getId(), other.getId()).isEquals();
    }
}
