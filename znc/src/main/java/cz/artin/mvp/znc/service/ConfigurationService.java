package cz.artin.mvp.znc.service;

import javax.portlet.PortletRequest;

import cz.artin.mvp.znc.model.ZncConfiguration;

/**
 * Service for manipulating configuration.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface ConfigurationService {
    /**
     * Reread configuration from the DDL. If there is no configuration DDL in
     * the system yet create default one.
     */
    ZncConfiguration initConfiguration(PortletRequest request);

    /**
     * Get already initialized configuration. Can be null.
     */
    ZncConfiguration getConfiguration();

    /**
     * Get configuration. Configuration will be read/created if initialization
     * is needed.
     */
    ZncConfiguration getConfiguration(PortletRequest request);
}
