package cz.artin.mvp.znc.service;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.util.mail.MailEngine;
import com.liferay.util.mail.MailEngineException;

import cz.artin.mvp.znc.model.ZncClaim;

/**
 * Represents mail queue handler. Sends mails to o2 address.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
@Service
public class MailSenderServiceImpl implements MailSenderService {
    private Log log = LogFactoryUtil.getLog(MailSenderServiceImpl.class.getName());

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${email.o2.template.path}'))}")
    private String templateO2;

    @Value("#{T(org.apache.commons.io.IOUtils).toString(" + "T(org.springframework.util.ResourceUtils).getURL("
            + "'${email.customer.template.path}'))}")
    private String templateCustomer;

    @Autowired
    private ConfigurationService configurationService;

    @Override
    public void sendMailO2(String subject, ZncClaim claim) {
        if (!StringUtils.hasText(subject)) {
            throw new MailParseException("Subject cannot be empty");
        }
        if (claim == null) {
            throw new MailParseException("Claim is null.");
        }

        String mailBody = renderMailO2Template(claim);

        MailMessage mailMessage = new MailMessage();
        mailMessage.setHTMLFormat(false);
        mailMessage.setSubject(subject);
        mailMessage.setBody(mailBody);

        try {
            mailMessage.setFrom(
                    new InternetAddress(configurationService.getConfiguration().getMailFrom(), "Záruka nejnižší ceny"));
            mailMessage.setTo(new InternetAddress(configurationService.getConfiguration().getMailTo()));
            log.debug("Sending mail to o2");
            MailEngine.send(mailMessage);
        } catch (UnsupportedEncodingException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (AddressException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (MailEngineException e) {
            throw new MailSendException("Problem sending mail", e);
        }
    }

    @Override
    public void sendMailCustomer(String subject, ZncClaim claim) throws MailException {
        if (!StringUtils.hasText(subject)) {
            throw new MailParseException("Subject cannot be empty");
        }
        if (claim == null) {
            throw new MailParseException("Claim is null.");
        }

        String mailBody = renderMailCustomerTemplate(claim);

        MailMessage mailMessage = new MailMessage();
        mailMessage.setHTMLFormat(false);
        mailMessage.setSubject(subject);
        mailMessage.setBody(mailBody);

        try {
            mailMessage.setFrom(
                    new InternetAddress(configurationService.getConfiguration().getMailFrom(), "Záruka nejnižší ceny"));
            mailMessage.setTo(new InternetAddress(claim.getCustomerEmail()));
            log.debug("Sending mail to o2");
            MailEngine.send(mailMessage);
        } catch (UnsupportedEncodingException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (AddressException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (MailEngineException e) {
            throw new MailSendException("Problem sending mail", e);
        }
    }

    /**
     * Render template - replace placeholders in the template by given values.
     */
    protected String renderMailO2Template(ZncClaim claim) {
        String result = templateO2.replaceAll("\\$\\$DATE\\$\\$", DATE_FORMAT.format(claim.getDate()))
                .replaceAll("\\$\\$FIRST_NAME\\$\\$", claim.getFirstName())
                .replaceAll("\\$\\$LAST_NAME\\$\\$", claim.getLastName())
                .replaceAll("\\$\\$PHONE_NUMBER\\$\\$", claim.getCustomerPhone())
                .replaceAll("\\$\\$EMAIL\\$\\$", claim.getCustomerEmail())
                .replaceAll("\\$\\$PRICE\\$\\$", claim.getPrice().toPlainString())
                .replaceAll("\\$\\$PRODUCER\\$\\$", claim.getPhoneProducer())
                .replaceAll("\\$\\$MODEL\\$\\$", claim.getPhoneModel()).replaceAll("\\$\\$SHOP\\$\\$", claim.getShop())
                .replaceAll("\\$\\$TARIFF\\$\\$", claim.getTariff())
                .replaceAll("\\$\\$COMPETITOR\\$\\$", claim.getCompetitor())
                .replaceAll("\\$\\$BUY_DATE\\$\\$", DATE_FORMAT.format(claim.getBuyDate()));
        return result;
    }

    /**
     * Render template - replace placeholders in the template by given values.
     */
    protected String renderMailCustomerTemplate(ZncClaim claim) {
        String result = templateCustomer.replaceAll("\\$\\$DATE\\$\\$", DATE_FORMAT.format(claim.getDate()))
                .replaceAll("\\$\\$FIRST_NAME\\$\\$", claim.getFirstName())
                .replaceAll("\\$\\$LAST_NAME\\$\\$", claim.getLastName())
                .replaceAll("\\$\\$PHONE_NUMBER\\$\\$", claim.getCustomerPhone())
                .replaceAll("\\$\\$EMAIL\\$\\$", claim.getCustomerEmail())
                .replaceAll("\\$\\$PRICE\\$\\$", claim.getPrice().toPlainString())
                .replaceAll("\\$\\$PRODUCER\\$\\$", claim.getPhoneProducer())
                .replaceAll("\\$\\$MODEL\\$\\$", claim.getPhoneModel()).replaceAll("\\$\\$SHOP\\$\\$", claim.getShop())
                .replaceAll("\\$\\$TARIFF\\$\\$", claim.getTariff())
                .replaceAll("\\$\\$COMPETITOR\\$\\$", claim.getCompetitor())
                .replaceAll("\\$\\$BUY_DATE\\$\\$", DATE_FORMAT.format(claim.getBuyDate()));
        return result;
    }
}
