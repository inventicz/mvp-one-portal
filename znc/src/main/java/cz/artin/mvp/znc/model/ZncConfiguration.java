package cz.artin.mvp.znc.model;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Represents holder for configuration values. Values are read from DDLs.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class ZncConfiguration implements Serializable {
    private static final long serialVersionUID = 1L;

    private String mailFrom;

    private String mailTo;

    private List<String> competitors;

    private Map<String, Set<String>> phones;

    private List<String> tariffs;

    private String firstNameErrorText;
    private String lastNameErrorText;
    private String phoneErrorText;
    private String mailErrorText;
    private String competitorErrorText;
    private String phoneProducerErrorText;
    private String imeiDuplicityErrorText;
    private String imeiErrorText;
    private String tariffErrorText;

    public String getTariffErrorText() {
        return tariffErrorText;
    }

    public void setTariffErrorText(String tariffErrorText) {
        this.tariffErrorText = tariffErrorText;
    }

    public String getImeiErrorText() {
        return imeiErrorText;
    }

    public void setImeiErrorText(String imeiErrorText) {
        this.imeiErrorText = imeiErrorText;
    }

    private String dateErrorText;
    private String phoneModelErrorText;
    private String priceErrorText;
    private String shopErrorText;

    public String getLastNameErrorText() {
        return lastNameErrorText;
    }

    public String getPhoneModelErrorText() {
        return phoneModelErrorText;
    }

    public void setPhoneModelErrorText(String phoneModelErrorText) {
        this.phoneModelErrorText = phoneModelErrorText;
    }

    public String getPriceErrorText() {
        return priceErrorText;
    }

    public void setPriceErrorText(String priceErrorText) {
        this.priceErrorText = priceErrorText;
    }

    public String getShopErrorText() {
        return shopErrorText;
    }

    public void setShopErrorText(String shopErrorText) {
        this.shopErrorText = shopErrorText;
    }

    public void setLastNameErrorText(String lastNameErrorText) {
        this.lastNameErrorText = lastNameErrorText;
    }

    public String getPhoneErrorText() {
        return phoneErrorText;
    }

    public void setPhoneErrorText(String phoneErrorText) {
        this.phoneErrorText = phoneErrorText;
    }

    public String getMailErrorText() {
        return mailErrorText;
    }

    public void setMailErrorText(String mailErrorText) {
        this.mailErrorText = mailErrorText;
    }

    public String getCompetitorErrorText() {
        return competitorErrorText;
    }

    public void setCompetitorErrorText(String competitorErrorText) {
        this.competitorErrorText = competitorErrorText;
    }

    public String getPhoneProducerErrorText() {
        return phoneProducerErrorText;
    }

    public void setPhoneProducerErrorText(String phoneProducerErrorText) {
        this.phoneProducerErrorText = phoneProducerErrorText;
    }

    public String getImeiDuplicityErrorText() {
        return imeiDuplicityErrorText;
    }

    public void setImeiDuplicityErrorText(String imeiDuplicityErrorText) {
        this.imeiDuplicityErrorText = imeiDuplicityErrorText;
    }

    public String getDateErrorText() {
        return dateErrorText;
    }

    public void setDateErrorText(String dateErrorText) {
        this.dateErrorText = dateErrorText;
    }

    public String getFirstNameErrorText() {
        return firstNameErrorText;
    }

    public void setFirstNameErrorText(String firstNameErrorText) {
        this.firstNameErrorText = firstNameErrorText;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String o2mailFrom) {
        this.mailFrom = o2mailFrom;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public List<String> getCompetitors() {
        return competitors;
    }

    public void setCompetitors(List<String> competitors) {
        this.competitors = competitors;
    }

    public Map<String, Set<String>> getPhones() {
        return phones;
    }

    public void setPhones(Map<String, Set<String>> phones) {
        this.phones = phones;
    }

    public List<String> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<String> tariffs) {
        this.tariffs = tariffs;
    }

}
