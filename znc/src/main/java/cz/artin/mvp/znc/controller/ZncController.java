package cz.artin.mvp.znc.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import cz.artin.mvp.znc.model.ZncClaimDto;
import cz.artin.mvp.znc.model.ZncConfiguration;
import cz.artin.mvp.znc.service.ConfigurationService;
import cz.artin.mvp.znc.service.ZncService;

/**
 * Represents controller for ZNC portlet.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Controller
@RequestMapping(value = "VIEW")
public class ZncController {
    private static final String BINDING_RESULT_ATTRIBUTE = "org.springframework.validation.BindingResult.zncClaimDto";

    private static final long MILLIS_PER_DAY = 24 * 60 * 60 * 1000;

    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    private Log log = LogFactoryUtil.getLog(ZncController.class);

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private ZncService zncService;

    /**
     * Default render method.
     */
    @RenderMapping
    public ModelAndView showStart(Model model, RenderRequest request, RenderResponse response) {
        log.debug("Displaying portlet");
        final ModelAndView result = new ModelAndView("start");

        final ZncClaimDto zncClaim;
        if (model.containsAttribute("zncClaimDto")) {
            zncClaim = (ZncClaimDto) model.asMap().get("zncClaimDto");
        } else {
            zncClaim = new ZncClaimDto();
        }

        ZncConfiguration configuration = configurationService.getConfiguration();
        if (configuration == null) {
            configuration = configurationService.initConfiguration(request);
        }

        Map<String, Set<String>> phoneModelsMap = configuration.getPhones();
        result.addObject("phoneProducers", phoneModelsMap.keySet());
        result.addObject("phoneModelsMap", phoneModelsMap);
        result.addObject("tariffs", configuration.getTariffs());
        result.addObject("competitors", configuration.getCompetitors());
        result.addObject("zncClaimDto", zncClaim);
        return result;
    }

    /** Handle sending ZNC claim. */
    @ActionMapping("handleZncClaimForm")
    public void handleZncClaimForm(@ModelAttribute ZncClaimDto zncClaim, BindingResult bindingResultTodel,
            ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
        log.debug("ZNC Claim received");

        BindingResult bindingResult = new BeanPropertyBindingResult(zncClaim, "zncClaimDto");
        // validation
        if (!StringUtils.hasText(zncClaim.getFirstName())) {
            bindingResult.rejectValue("firstName", null,
                    configurationService.getConfiguration().getFirstNameErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getLastName())) {
            bindingResult.rejectValue("lastName", null, configurationService.getConfiguration().getLastNameErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getShop())) {
            bindingResult.rejectValue("shop", null, configurationService.getConfiguration().getShopErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getPhoneImei())) {
            bindingResult.rejectValue("phoneImei", null, configurationService.getConfiguration().getImeiErrorText());
        } else {
            if (zncService.existsClaimWithImei(zncClaim.getPhoneImei())) {
                bindingResult.rejectValue("phoneImei", null,
                        configurationService.getConfiguration().getImeiDuplicityErrorText());
            }
        }
        if (!StringUtils.hasText(zncClaim.getCustomerPhone())) {
            bindingResult.rejectValue("customerPhone", null,
                    configurationService.getConfiguration().getPhoneErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getCustomerEmail())) {
            bindingResult.rejectValue("customerEmail", null,
                    configurationService.getConfiguration().getMailErrorText());
        }
        if (zncClaim.getPrice() == null) {
            bindingResult.rejectValue("price", null, configurationService.getConfiguration().getPriceErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getPhoneProducer())) {
            bindingResult.rejectValue("phoneProducer", null,
                    configurationService.getConfiguration().getPhoneProducerErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getPhoneModel())) {
            bindingResult.rejectValue("phoneModel", null,
                    configurationService.getConfiguration().getPhoneModelErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getTariff())) {
            bindingResult.rejectValue("tariff", null, configurationService.getConfiguration().getTariffErrorText());
        }
        if (!StringUtils.hasText(zncClaim.getCompetitor())) {
            bindingResult.rejectValue("competitor", null,
                    configurationService.getConfiguration().getCompetitorErrorText());
        }
        String dateString = zncClaim.getDateString();
        if (!StringUtils.hasText(dateString)) {
            bindingResult.rejectValue("dateString", null, configurationService.getConfiguration().getDateErrorText());
        } else {
            try {
                Date date = DATE_FORMAT.parse(dateString);
                zncClaim.setDate(date);
                long today = new Date().getTime() / MILLIS_PER_DAY;
                long dateDay = date.getTime() / MILLIS_PER_DAY;
                long difference = today - dateDay;
                if ((difference > 7) || (difference < 0)) {
                    bindingResult.rejectValue("dateString", null,
                            configurationService.getConfiguration().getDateErrorText());
                }
            } catch (ParseException e) {
                bindingResult.rejectValue("dateString", null,
                        configurationService.getConfiguration().getDateErrorText());
            }

        }

        if (!bindingResult.hasErrors()) {
            String currentCompleteUrl = PortalUtil.getCurrentURL(actionRequest);
            String currentUrl = currentCompleteUrl.substring(0, currentCompleteUrl.lastIndexOf("?"));
            zncService.createZncClaim(zncClaim, configurationService.getConfiguration(actionRequest), currentUrl);
            actionResponse.setRenderParameter("action", "finish");
        } else {
            zncClaim.setPhoneProducer(null);
            zncClaim.setPhoneModel(null);
            model.addAttribute(BINDING_RESULT_ATTRIBUTE, bindingResult);
        }
    }

    /**
     * Default render method.
     */
    @RenderMapping(params = "action=finish")
    public ModelAndView showFinish(RenderRequest request, RenderResponse response) {
        log.debug("End of webflow");
        ModelAndView result = new ModelAndView("finish");

        return result;
    }
}
