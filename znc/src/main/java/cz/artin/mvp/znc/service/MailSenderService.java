package cz.artin.mvp.znc.service;

import org.springframework.mail.MailException;

import cz.artin.mvp.znc.model.ZncClaim;

/**
 * Mail queue handler. Sends mails to o2 and customer.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
public interface MailSenderService {

    /**
     * Render and send mail to o2.
     * @param claim message model
     * @throws MailException when mail sending was not successful
     */
    void sendMailO2(String subject, ZncClaim claim) throws MailException;

    /**
     * Render and send mail to customer.
     * @param claim message model
     * @throws MailException when mail sending was not successful
     */
    void sendMailCustomer(String subject, ZncClaim claim) throws MailException;

}
