package cz.artin.mvp.znc.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.znc.model.ZncClaim;

/**
 * Represents high level services for mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

    private Log log = LogFactoryUtil.getLog(NotificationServiceImpl.class.getName());

    @Autowired
    private ZncService zncService;

    @Autowired
    private MailSenderService mailSenderService;

    @Transactional
    @Override
    @Scheduled(fixedRate = 60000)
    public void sendEmailsO2() {
        log.debug("Sending notification mails to O2");
        List<ZncClaim> claimsToSend = zncService.findO2EmailToSend();

        for (ZncClaim claim : claimsToSend) {
            sendEmailO2(claim);
        }
    }

    @Transactional
    @Override
    @Scheduled(initialDelay = 30000, fixedRate = 60000)
    public void sendEmailsCustomer() {
        log.debug("Sending notification mails to Customers");
        List<ZncClaim> claimsToSend = zncService.findCustomerEmailToSend();

        for (ZncClaim claim : claimsToSend) {
            sendEmailCustomer(claim);
        }
    }

    /**
     * Forward single {@link ZncClaim} to the O2 mail.
     *
     * @return {@link ZncClaim} updated regarding result of the sending
     */
    private ZncClaim sendEmailO2(ZncClaim claim) {
        log.info("Sending notification mail to O2 " + claim.getId());

        boolean messageError = false;
        try {
            String mailSubject = "​Reakce Záruka nejniší ceny: " + claim.getId() + " , tel. cislo: "
                    + claim.getCustomerPhone();

            mailSenderService.sendMailO2(mailSubject, claim);
        } catch (MailException e) {
            messageError = true;
            log.warn("Mail " + claim + " was not sent.", e);
            claim.setO2Tries(claim.getO2Tries() + 1);
            claim.setO2MailLastTryDate(new Date());
        }

        if (!messageError) {
            Date now = new Date();
            if (log.isDebugEnabled()) {
                log.debug("Mail " + claim.getId() + " sent " + now);
            }
            claim.setO2MailSentDate(now);
        }

        return claim;
    }

    /**
     * Forward single {@link ZncClaim} to the customer's mail.
     *
     * @return {@link ZncClaim} updated regarding result of the sending
     */
    private ZncClaim sendEmailCustomer(ZncClaim claim) {
        log.info("Sending notification mail to customer " + claim.getId());

        boolean messageError = false;
        try {
            String mailSubject = "​Záruka nejniší ceny: ";

            mailSenderService.sendMailCustomer(mailSubject, claim);
        } catch (MailException e) {
            messageError = true;
            log.warn("Mail " + claim + " was not sent.", e);
            claim.setCustomerTries(claim.getCustomerTries() + 1);
            claim.setCustomerMailLastTryDate(new Date());
        }

        if (!messageError) {
            Date now = new Date();
            if (log.isDebugEnabled()) {
                log.debug("Mail " + claim.getId() + " sent " + now);
            }
            claim.setCustomerMailSentDate(now);
        }

        return claim;
    }

    public void setMailSenderService(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }
}
