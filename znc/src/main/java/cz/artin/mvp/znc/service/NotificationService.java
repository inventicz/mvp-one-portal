package cz.artin.mvp.znc.service;

/**
 * High level services for mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
public interface NotificationService {

    /**
     * Send emails to O2. Usually called by scheduler.
     */
    void sendEmailsO2();

    /**
     * Send emails to O2. Usually called by scheduler.
     */
    void sendEmailsCustomer();
}
