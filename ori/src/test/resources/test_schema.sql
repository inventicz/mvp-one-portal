create table ori_data_answer(
    id bigint not null auto_increment,
    datum datetime not null,
    guru boolean,
    is_helpful boolean,
    mail_last_try_date datetime,
    mail_sent_date datetime,
    message longtext,
    reason varchar(32),
    target_mail varchar(255),
    tries integer not null,
    url varchar(255),
    user_id varchar(255) not null,
    would_call boolean,
    primary key (id));
create index ori_data_answer_url_ix on ori_data_answer (url);
create index ori_data_answer_date_ix on ori_data_answer (datum);
