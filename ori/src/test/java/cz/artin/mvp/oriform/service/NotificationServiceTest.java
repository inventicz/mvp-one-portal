package cz.artin.mvp.oriform.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assume.assumeTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;
import cz.artin.mvp.dao.OriDataAnswerDao;
import cz.artin.mvp.model.OriDataAnswer;
import cz.artin.mvp.service.OriService;

/**
 * Represents basic tests for the service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class NotificationServiceTest extends AbstractTest {
    @Mock
    private MailSenderService mailSenderService;

    @Autowired
    @InjectMocks
    private NotificationServiceImpl notificationService;

    @Autowired
    private OriService oriService;

    @Autowired
    private OriDataAnswerDao answerDao;

    @Before
    public void setupMocks() {
        MockitoAnnotations.initMocks(this);
    }

    /** Test if sent guru mail updates answers properly. */
    @Test
    public void mailQueueHandling() {
        List<OriDataAnswer> answersToSend = oriService.findAnswersToSend();
        assumeTrue(answersToSend.size() == 10);
        for (OriDataAnswer answer : answersToSend) {
            assumeTrue(answer.getMailSentDate() == null);
        }

        notificationService.sendEmailsToGuru();

        for (OriDataAnswer answer : answersToSend) {
            OriDataAnswer foundAnswer = answerDao.findById(answer.getId());
            assertNotNull(answer.getMailSentDate());
            assertNotNull(foundAnswer.getMailSentDate());
        }
    }

    /** Test if low level mailing method was called properly. */
    @Test
    public void mailQueueMailSending() {
        assumeTrue(oriService.findAnswersToSend().size() == 10);

        notificationService.sendEmailsToGuru();

        verify(mailSenderService, times(10)).sendMail((String) any(), (OriDataAnswer) any());
    }

}
