package cz.artin.mvp.oriform.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Date;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;

/**
 * Represents tests for the service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class SenderServiceTemplateTest extends AbstractTest {

    @Autowired
    private MailSenderServiceImpl senderService;

    /** Test reading of template. */
    @Test
    public void readTemplate() {
        String template = senderService.getTemplate();

        assertTrue(template.startsWith("TEST MAIL START"));
        assertTrue(template.endsWith("TEST MAIL END\n"));
    }

    /** Test rendering of template. */
    @Test
    public void renderTemplate() {
        @SuppressWarnings("deprecation")
        Date testDate = new Date(115, 1, 28, 11, 12, 13);
        String mail = senderService.renderMailTemplate("ABCDEF", "REASON", testDate);

        assertTrue(mail.startsWith("TEST MAIL START"));
        assertTrue(mail.endsWith("TEST MAIL END\n"));

        assertTrue(mail.contains("ABCDEF"));
        assertTrue(mail.contains("REASON"));
        assertTrue(mail.contains("28. 02. 2015 11:12"));

        assertFalse(mail.contains("$$BODY$$"));
        assertFalse(mail.contains("$$REASON$$"));
        assertFalse(mail.contains("$$DATE$$"));
    }

}
