package cz.artin.mvp.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;
import cz.artin.mvp.model.OriDataAnswer;

/**
 * Test for guru mail related functionality of dao.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class OriServiceXlsRenderingTest extends AbstractTest {
    @Autowired
    private OriService service;

    @Test
    public void nullReplacement() {
        List<OriDataAnswer> answers = new ArrayList<OriDataAnswer>();
        OriDataAnswer answer = new OriDataAnswer();
        answers.add(answer);

        String resultXls = service.renderExportTable(answers).replaceAll("\n", "");
        assertFalse(resultXls.contains("null"));
        assertTrue(resultXls.matches(".*---.*---.*---.*---.*"));
    }
}
