package cz.artin.mvp.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;
import cz.artin.mvp.model.OriDataAnswer;

/**
 * Test for guru mail related functionality of dao.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class OriServiceGuruCheckingTest extends AbstractTest {
    @Autowired
    private OriServiceImpl service;

    @Test
    public void simple() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage("abcdef");
        assertFalse(service.checkGuruMessage(answer));
        answer.setMessage("777");
        assertTrue(service.checkGuruMessage(answer));
        answer.setMessage("abcdef@example.com");
        assertTrue(service.checkGuruMessage(answer));
    }

    @Test
    public void multiline() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage("abcdef\r\nbcs");
        assertFalse(service.checkGuruMessage(answer));
        answer.setMessage("abcdef\r\n777");
        assertTrue(service.checkGuruMessage(answer));
        answer.setMessage("abcdef\r\nabcdef@example.com");
        assertTrue(service.checkGuruMessage(answer));
    }

    @Test
    public void nullMessage() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage(null);
        assertFalse(service.checkGuruMessage(answer));
    }

    @Test
    public void emptyMessage() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage("");
        assertFalse(service.checkGuruMessage(answer));
    }

    @Test
    public void whitespace() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage("abc def");
        assertFalse(service.checkGuruMessage(answer));
        answer.setMessage("77 7");
        assertTrue(service.checkGuruMessage(answer));
        answer.setMessage("abcdef @ example.com");
        assertTrue(service.checkGuruMessage(answer));
    }

    @Test
    public void longer() {
        OriDataAnswer answer = new OriDataAnswer();
        answer.setMessage("kljhkjhh 888888  ěščřřžřžýřý lkjlkjlkjlkj kljhkjhh 888888 lkjlkjlkjlkjkljhkjhh 888888 lkjlkjlkjlkjkljhkjhh 888888 lkjlkjlkjlkjkljhkjhh 888888 lkjlkjlkjlkjkljhkjhh 888888 lkjlkjlkjlkjkljhkjhh 888888 lkjlkjlkjlkj");
        assertTrue(service.checkGuruMessage(answer));
    }

}
