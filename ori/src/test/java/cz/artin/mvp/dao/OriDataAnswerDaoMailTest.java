package cz.artin.mvp.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;

/**
 * Test for guru mail related functionality of dao.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class OriDataAnswerDaoMailTest extends AbstractTest{

    @Autowired
    private OriDataAnswerDao answerDao;

    @Test
    public void count() {
        assertEquals(15, answerDao.findAnswersToSend(0, 0).size());
    }
}
