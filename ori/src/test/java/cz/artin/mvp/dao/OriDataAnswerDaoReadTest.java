package cz.artin.mvp.dao;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.AbstractTest;
import cz.artin.mvp.model.OriDataAnswer;

/**
 * Test for basic functionality of dao.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class OriDataAnswerDaoReadTest extends AbstractTest{

    @Autowired
    private OriDataAnswerDao answerDao;

    @Test
    public void count() {
        assertEquals(1999, answerDao.count());
    }

    @Test
    public void readAnswers10() {
        List<OriDataAnswer> answers = answerDao.readAnswers(0, 10, 0, null, false, false);
        assertEquals(10, answers.size());
    }

    @Test
    public void readAnswers1000() {
        List<OriDataAnswer> answers = answerDao.readAnswers(0, 1000, 0, null, false, false);
        assertEquals(1000, answers.size());
    }

    @Test
    public void readAnswers1000SecondPage() {
        List<OriDataAnswer> answers = answerDao.readAnswers(1, 1000, 0, null, false, false);
        assertEquals(999, answers.size());
    }

    @Test
    public void readAnswers1000ThirdPage() {
        List<OriDataAnswer> answers = answerDao.readAnswers(2, 1000, 0, null, false, false);
        assertEquals(0, answers.size());
    }

    @Test
    public void readAnswers1000InvalidSection() {
        List<OriDataAnswer> answers = answerDao.readAnswers(0, 1000, 0, "xyz", false, false);
        assertEquals(1000, answers.size());
    }

    @Test
    public void readAnswers1000FaqSection() {
        List<OriDataAnswer> answers = answerDao.readAnswers(0, 1000, 0, "faq", false, false);
        assertEquals(37, answers.size());
    }

    @Test
    public void readAllAnswers() {
        List<OriDataAnswer> answers = answerDao.readAnswers(0, 0, 0, null, false, false);
        assertEquals(1999, answers.size());
    }
}
