package com.pinkfuture.sc.core.notification;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.SendFailedException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang.SerializationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.pinkfuture.sc.api.configuration.Config;
import com.pinkfuture.sc.api.exceptions.SystemException;
import com.pinkfuture.sc.core.sms.SMS;
import com.pinkfuture.sc.core.sms.SMSService;
import com.pinkfuture.sc.persist.domain.message.Attachment;
import com.pinkfuture.sc.persist.domain.message.Message;
import com.pinkfuture.sc.persist.domain.message.Message.MessageType;
import com.pinkfuture.sc.persist.domain.message.dao.MessageDao;
import com.sun.mail.smtp.SMTPAddressFailedException;

/**
 * @author Libor Ondrusek
 */
@Service
public class NotificationSender {

	private static final Logger log = LoggerFactory.getLogger(NotificationSender.class);

	@Inject
	private MessageDao messageDao;
	@Inject
	private JavaMailSender mailSender;
	@Inject
	private Config config;
	@Inject
	private SMSService smsService;

	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	public void sendEmail() {
		Boolean emailEnabled = config.getBoolean(Config.ApplicationSettings.EMAIL_SENDER_ENABLED);
		if (emailEnabled) {
			log.info("Obtaining saved emails with reised due date");
			List<Message> messages = messageDao.getAllUnsendedActualMessages(MessageType.EMAIL);
			log.debug("Loaded {} email messages", messages.size());
			for (Message message : messages) {
				sendEmail(message);
			}
		}
		else {
			log.info("Email sender is disabled!");
		}
	}
	
	@Scheduled(initialDelay = 0, fixedDelay = 60000)
	public void sendSMS(){
		Boolean smsEnabled = config.getBoolean(Config.ApplicationSettings.SMS_SENDER_ENABLED);
		if (smsEnabled){
			log.info("Obtaining saved sms with reised due date");
			List<Message> messages = messageDao.getAllUnsendedActualMessages(MessageType.SMS);
			log.debug("Loaded {} email messages", messages.size());
			for (Message message : messages) {
				sendSMS(message);
			}
		}
		else{
			log.info("SMS sender is disabled!");
		}
	}

	@Transactional
	private void sendSMS(Message message){
		try{
			SMS sms = new SMS(message.getTarget(), message.getContent());
			smsService.send(sms);
			message.setSentDate(new Date());
			messageDao.update(message);
		}
		catch(RuntimeException e){
			log.error("SMS service was not able to send SMS (message has id={} in sc_message), we will try to send it later.",message.getId());
			throw new SystemException("Error sending sms", e);
		}
	}
	
	@Transactional
	private void sendEmail(Message message) {
		checkNotNull(message, "message must not be null");
		MimeMessage email = mailSender.createMimeMessage();
		MimeMessageHelper helper;
		try {
			helper = new MimeMessageHelper(email, true);
			helper.setTo(message.getTargets());
			String[] copyTargets = message.getCopyTargets();
			if (copyTargets.length > 0) {
				helper.setCc(copyTargets);
			}
			helper.setFrom(config.getString(Config.ApplicationSettings.EMAIL_SENDER_ADDRESS));
			helper.setSubject(message.getSubject());
			helper.setText(message.getContent());
			for (Attachment attachment : message.getAttachments()) {
				Object deserialized = SerializationUtils.deserialize(Base64.decode(attachment.getContent().getBytes()));
				if (deserialized instanceof File) {
					File file = (File) deserialized;
					helper.addAttachment(attachment.getName(), file);
				}
				else {
					throw new SystemException("Serialized attachment is not a File object");
				}
			}
		}
		catch (MessagingException e) {
			throw new SystemException("Error sending email", e);
		}

		boolean mailSendError = false;
		try {
			mailSender.send(email);
			log.info("Email sent, sc_message id: {}, target = {}", message.getId(), message.getTarget());
		}
		catch (MailSendException e) {
			log.error("MailSendException found.",e);
			mailSendError = true;
	        Exception[] exceptionArray = e.getMessageExceptions();
	        e.getFailedMessages();
	        for(Exception e1 : exceptionArray){
	            if(e1 instanceof SendFailedException){
	                Exception e2 = ((SendFailedException)e1).getNextException();
	                if(e2 instanceof SMTPAddressFailedException){
	                	mailSendError = false;
	                    log.error("Caught SMTPAddressFailedException. Invalid email user in DB",e2);
	                    break;
	                }
	            }
	        }
			
		}
		if (!mailSendError){
			message.setSentDate(new Date());
			messageDao.update(message);
		}
	}
}
 
