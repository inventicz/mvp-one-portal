package cz.artin.mvp.oriform.service;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.util.mail.MailEngine;
import com.liferay.util.mail.MailEngineException;

import cz.artin.mvp.model.OriDataAnswer;

/**
 * Represents mail queue handler. Sends mails to guru address.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
@Service
public class MailSenderServiceImpl implements MailSenderService {
    private Log log = LogFactoryUtil.getLog(NotificationServiceImpl.class.getName());

    @Value("${email.address.from}")
    private String fromAddress;

    @Value("${email.address.to}")
    private String toAddress;

    @Value("#{T(org.apache.commons.io.IOUtils).toString("
                + "T(org.springframework.util.ResourceUtils).getURL("
                    + "'${email.template.path}'))}")
    private String template;

    @Value("${email.template.date_format}")
    private String dateFormatDef;

    private DateFormat dateFormat;

    @PostConstruct
    public void init() {
        this.dateFormat = new SimpleDateFormat(dateFormatDef);
    }

    @Override
    public void sendMail(String subject, OriDataAnswer answer) {
        if (!StringUtils.hasText(subject)) {
            throw new MailParseException("Subject cannot be empty");
        }
        if (answer == null) {
            throw new MailParseException("Answer is null.");
        }

        String mailBody = renderMailTemplate(answer.getMessage(), answer.getReason(), answer.getDate());

        MailMessage mailMessage = new MailMessage();
        mailMessage.setHTMLFormat(false);
        mailMessage.setSubject(subject);
        mailMessage.setBody(mailBody);

        try {
            mailMessage.setFrom(new InternetAddress(fromAddress, "Portal Unimed"));
            mailMessage.setTo(new InternetAddress(toAddress));
            log.debug("Sending mail to guru");
            MailEngine.send(mailMessage);
        } catch (UnsupportedEncodingException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (AddressException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (MailEngineException e) {
            throw new MailSendException("Problem sending mail", e);
        }

    }

    /**
     * Render template - replace placeholders in the template by given values.
     * <ul>
     *         <li>$$BODY$$</li>
     *         <li>$$REASON$$</li>
     *         <li>$$DATE$$</li>
     * </ul>
     */
    protected String renderMailTemplate(String mailBody, String reason, Date sentDate) {
        final String mailBodyForTemplate;
        final String reasonForTemplate;
        final String dateForTemplate;

        if (StringUtils.isEmpty(mailBody)) {
            mailBodyForTemplate = "---";
        } else {
            mailBodyForTemplate = mailBody;
        }

        if (StringUtils.isEmpty(reason)) {
            reasonForTemplate = "---";
        } else {
            reasonForTemplate = reason;
        }

        if (sentDate == null) {
            dateForTemplate = "---";
        } else {
            dateForTemplate = dateFormat.format(sentDate);
        }

        String result = template.replaceAll("\\$\\$BODY\\$\\$", mailBodyForTemplate)
                .replaceAll("\\$\\$REASON\\$\\$", reasonForTemplate)
                .replaceAll("\\$\\$DATE\\$\\$", dateForTemplate);
        return result;
    }

    protected String getTemplate() {
        return template;
    }
}
