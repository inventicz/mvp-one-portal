package cz.artin.mvp.oriform.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.model.OriDataAnswer;
import cz.artin.mvp.service.OriService;

/**
 * Represents high level services for guru mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

    private Log log = LogFactoryUtil.getLog(NotificationServiceImpl.class.getName());

    @Autowired
    private OriService oriService;

    @Autowired
    private MailSenderService mailSenderService;

    @Value("${email.template.subject}")
    private String mailSubject;

    @Transactional
    @Override
    public void sendEmailsToGuru() {
        log.info("Sending notification mails to guru");
        List<OriDataAnswer> answersToSend = oriService.findAnswersToSend();

        for (OriDataAnswer answer : answersToSend) {
            sendEmailToGuru(answer);
        }
    }

    /**
     * Forward single {@link OriDataAnswer} to the guru mail.
     * @return {@link OriDataAnswer} updated regarding result of the sending
     */
    private OriDataAnswer sendEmailToGuru(OriDataAnswer answer) {
        log.info("Sending notification mail to guru (" + answer.getUserId() + ")");

        boolean messageError = false;
        try {
            mailSenderService.sendMail(mailSubject, answer);
        } catch (MailException e) {
            messageError = true;
            log.warn("Mail " + answer + " was not sent.", e);
            answer.setTries(answer.getTries() + 1);
            answer.setMailLastTryDate(new Date());
        }

        if (!messageError) {
            answer.setMailSentDate(new Date());
        }

        return answer;
    }

    public void setMailSenderService(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }
}
