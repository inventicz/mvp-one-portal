package cz.artin.mvp.oriform.service;

/**
 * High level services for guru mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
public interface NotificationService {

    /**
     * Send email to guru. Usually called by scheduler.
     */
    void sendEmailsToGuru();
}
