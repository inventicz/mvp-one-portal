package cz.artin.mvp.oriform.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

import cz.artin.mvp.model.OriDataAnswer;
import cz.artin.mvp.service.OriService;

/**
 * Main controller for Ori form application.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Controller
@RequestMapping(value = "VIEW")
public class OriController {
    private Log log = LogFactoryUtil.getLog(OriController.class.getName());

    @Autowired
    private OriService oriService;

    @Value("${web.external.chat}")
    private String chatUrl;

    /**
     * When the portlet is added to the portal page this method will be called.
     * So this is the default render method.
     */
    @RenderMapping
    public String showStart(RenderRequest request, RenderResponse response) {
        log.debug("Displaying portlet.");
        return "start";
    }

    /** Handle "Is page useful" answer. */
    @ActionMapping(value = "handleFirstAnswer")
    public void handleFirstAnswer(@RequestParam(value = "isHelpful", required = false) Boolean isHelpful, ActionRequest actionRequest,
            ActionResponse actionResponse) {
        log.debug("Handling first answer");

        if (isHelpful == null) {
            log.debug("Required parameter is not present, returning to start");
        } else {

            OriDataAnswer answer = new OriDataAnswer();
            User user = (User) actionRequest.getAttribute(WebKeys.USER);
            final String userId;
            if ((user == null) || (user.getUuid() == null)) {
                userId = "NA";
            } else {
                userId = user.getUuid();
            }

            String currentCompleteUrl = PortalUtil.getCurrentURL(actionRequest);
            String currentUrl = currentCompleteUrl.substring(0, currentCompleteUrl.lastIndexOf("?"));

            //page id
            //ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
            //String pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale());


            this.oriService.createAnswer(answer, userId, isHelpful, currentUrl);

            actionResponse.setRenderParameter("answerId", answer.getId().toString());
            if (isHelpful) {
                actionResponse.setRenderParameter("action", "pageIsHelpful");
            } else {
                actionResponse.setRenderParameter("action", "pageNotHelpful");
            }
        }
    }

    /** Display page is helpful content. */
    @RenderMapping(params = "action=pageIsHelpful")
    public String showPageHelpful(@RequestParam("answerId") String answerId, ModelMap model) {
        log.debug("Page is helpful reaction");
        model.addAttribute("answerId", answerId);
        return "pageIsHelpful";
    }

    /** Handle answer for question "would you call us". */
    @ActionMapping(value = "handleWouldCall")
    public void handleWouldCall(@RequestParam(value = "answerId", required = false) Long answerId, @RequestParam(value = "wouldCall", required = false) Boolean wouldCall,
            ActionRequest actionRequest, ActionResponse actionResponse) {
        log.debug("Handling would call answer");

        if ((answerId == null) || (wouldCall == null)) {
            log.debug("Required parameter is not present, returning to start");
        } else {
            OriDataAnswer answer = oriService.findAnswer(answerId);
            if (answer == null) {
                log.error("Answer with id " + answerId + " was not found in DB!");
            }
            oriService.updateAnswer(answer, wouldCall, null, null, null);
            actionResponse.setRenderParameter("action", "thanks");
        }
    }

    /** Show final portlet content. */
    @RenderMapping(params = "action=thanks")
    public String showThanks(Model model, RenderRequest request) {
        log.debug("End of webflow");
        String showChat = request.getParameter("showChat");
        if ("true".equals(showChat)) {
            model.addAttribute("showChat", true);
            model.addAttribute("chatUrl", chatUrl);
        }
        return "thanks";
    }

    /** Display "page is not helpful" content. */
    @RenderMapping(params = "action=pageNotHelpful")
    public String showPageNotHelpful(@RequestParam("answerId") Long answerId, ModelMap model) {
        log.debug("Page is not helpful reaction");
        OriDataAnswer answer = oriService.findAnswer(answerId);
        if (answer == null) {
            log.error("Answer with id " + answerId + " was not found in DB!");
        }
        model.put("answer", answer);
        model.put("chatUrl", chatUrl);
        return "pageNotHelpful";
    }

    /** Handle main form content. */
    @ActionMapping(value = "handleNotHelpfulForm")
    public void handleNotHelpfulForm(@ModelAttribute OriDataAnswer answer, ActionRequest actionRequest,
            ActionResponse actionResponse) {
        log.debug("Handling question form");

        if (answer.getId() == null) {
            log.info("Required parameter is not present, returning to start");
        } else {
            if ("pomoc".equals(answer.getReason())) {
                actionResponse.setRenderParameter("showChat", "true");
            }

            OriDataAnswer answerPersisted = oriService.findAnswer(answer.getId());
            oriService.updateAnswer(answerPersisted, null, answer.getReason(), answer.getMessage(), null);

            oriService.sendToGuruIfNeeded(answerPersisted);
            actionResponse.setRenderParameter("action", "thanks");
        }
    }
}
