package cz.artin.mvp.oriform.service;

import org.springframework.mail.MailException;

import cz.artin.mvp.model.OriDataAnswer;

/**
 * Mail queue handler. Sends mails to guru address.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
public interface MailSenderService {

    /**
     * Render and send mail to guru.
     * @param answer message model
     * @throws MailException when mail sending was not successful
     */
    void sendMail(String subject, OriDataAnswer answer) throws MailException;
}
