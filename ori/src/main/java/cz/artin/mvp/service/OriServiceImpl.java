package cz.artin.mvp.service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.dao.OriDataAnswerDao;
import cz.artin.mvp.model.AnswerPageHolder;
import cz.artin.mvp.model.OriDataAnswer;

/**
 * Represents services for handling answers.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Service
public class OriServiceImpl implements OriService {
    private Log log = LogFactoryUtil.getLog(OriServiceImpl.class.getName());

    /** How many guru mail will be send in one cycle. */
    @Value("${email.queue.at_once}")
    private int answerToSendAtOnce;

    @Value("${email.guru.regexp}")
    private String sentToGuruRegexp;

    /** How many times will try server to send mail. */
    @Value("${email.queue.max_try}")
    private int tryLimit;

    @Autowired
    private OriDataAnswerDao oriDataAnswerDao;

    @Value("${email.template.date_format}")
    private String dateFormatDef;

    private DateFormat dateFormat;

    @PostConstruct
    public void init() {
        this.dateFormat = new SimpleDateFormat(dateFormatDef);
    }

    @Override
    @Transactional
    public void createAnswer(OriDataAnswer answer, String userId, Boolean isHelpful, String url) {
        final String urlToSave;
        if (url == null) {
            urlToSave = "---";
        } else if (url.length() > 255) {
            log.warn("URL had to be shortened URL: " + url);
            urlToSave = url.substring(0, 254);
        } else {
            urlToSave = url;
        }

        answer.setUserId(userId);
        answer.setDate(new Date());
        answer.setHelpful(isHelpful);
        answer.setUrl(urlToSave);
        log.info("Creating ORI answer: " + answer.toString());
        oriDataAnswerDao.create(answer);
    }

    @Override
    @Transactional(readOnly = true)
    public OriDataAnswer findAnswer(Long id) {
        log.debug("Searching for ORI answer with id " + id);
        return oriDataAnswerDao.findById(id);
    }

    @Override
    @Transactional
    public void updateAnswer(OriDataAnswer answer, Boolean wouldCall,
            String reason, String message, Boolean isForGuru) {
        if (wouldCall != null) {
            answer.setWouldCall(wouldCall);
        }
        if (reason != null) {
            answer.setReason(reason);
        }
        if (message != null) {
            if (message.length() > OriDataAnswer.MESSAGE_LENGTH) {
                answer.setMessage(message.substring(0, OriDataAnswer.MESSAGE_LENGTH));
            } else {
                answer.setMessage(message);
            }
        }
        if (isForGuru != null) {
            answer.setGuru(isForGuru);
        }

        log.debug("Updating ORI answer: " + answer.toString());
        oriDataAnswerDao.save(answer);
    }

    @Override
    @Transactional
    public void sendToGuruIfNeeded(OriDataAnswer answer) {
        if (answer.getMessage() == null || answer.getReason() == null) {
            throw new RuntimeException("Message in OriDataAnswer or Reason is null!");
        }

        boolean forGuru = checkGuruMessage(answer);

        if (forGuru) {
            updateAnswer(answer, null, null, null, true);
        }
    }

    /**
     * @return true if message should be sent to guru.
     */
    protected boolean checkGuruMessage(OriDataAnswer answer) {
        final boolean matches;

        String message = answer.getMessage();
        if (message != null) {
            matches = StringUtils.trimAllWhitespace(message).matches(sentToGuruRegexp);
        } else {
            matches = false;
        }
        return matches;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OriDataAnswer> findAnswers(Integer page, Integer pageSize, Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly) {
        //validation
        final int queryPage;
        if ((page == null) || (page < 0)) {
            queryPage = 0;
        } else {
            queryPage = page;
        }

        final int queryPageSize;
        if (pageSize == null) {
            queryPageSize = 200;
        } else {
            queryPageSize = pageSize;
        }

        final int queryDays;
        if ((days == null) || (days < 0)) {
            queryDays = 0;
        } else {
            queryDays = days;
        }

        List<OriDataAnswer> answers = oriDataAnswerDao.readAnswers(queryPage, queryPageSize, queryDays, urlFilter, notHelpfulOnly, messageOnly);

        return answers;
    }

    @Override
    @Transactional(readOnly = true)
    public int countAnswers(Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly) {
        int result = oriDataAnswerDao.countAnswers(days, urlFilter, notHelpfulOnly, messageOnly);
        return result;
    }

    @Override
    @Transactional
    public Long countAllAnswers() {
        return oriDataAnswerDao.count();
    }

    @Override
    public void validateParams(AnswerPageHolder answerParams) {
        log.debug("Validating export page command object.");

        //urlFilter
        String urlFilter = answerParams.getUrlFilter();
        if (StringUtils.hasText(urlFilter) && !"faq".equals(urlFilter)) {
            log.info("Invalid section id in export page command object.");
            answerParams.setUrlFilter("");
        }
    }

    @Override
    public String renderExportTable(Iterable<OriDataAnswer> answers) {
        StringBuilder result = new StringBuilder();
        result.append("<!DOCTYPE html>\n<head>\n<meta http-equiv='content-type' content='text/html; charset=UTF-8' />\n</head>\n<body>\n<table>\n");
        //table header
        result.append("<tr><th><strong>ID</strong></th><th><strong>Datum</strong></th><th>"
                + "<strong>URL</strong></th><th><strong>Text</strong></th><th><strong>GURU</strong></th></tr>\n");

        for (OriDataAnswer answer : answers) {
            final String guruStatus;
            Boolean guru = answer.getGuru();
            if ((guru == null) || (!guru)) {
                guruStatus = "";
            } else {
                guruStatus = "X";
            }

            final String id;
            if (answer.getId() == null) {
                id = "---";
            } else {
                id = answer.getId().toString();
            }

            final String dateString;
            Date date = answer.getDate();
            if (date == null) {
                dateString = "---";
            } else {
                if (dateFormat == null) {
                    dateString = "----";
                } else {
                    dateString = dateFormat.format(date);
                }
            }

            final String url;
            if (StringUtils.isEmpty(answer.getUrl())) {
                url = "---";
            } else {
                url = StringEscapeUtils.escapeHtml(answer.getUrl());
            }

            final String message;
            if (StringUtils.isEmpty(answer.getMessage())) {
                message = "---";
            } else {
                message = answer.getMessage();
            }

            result.append("<tr>");
            result.append("<td>").append(id).append("</td>\n");
            result.append("<td>").append(dateString).append("</td>\n");
            result.append("<td>").append(url).append("</td>\n");
            result.append("<td>").append(message).append("</td>\n");
            result.append("<td>").append(guruStatus).append("</td>\n");
            result.append("</tr>\n");
        }
        return result.toString();
    }

    @Override
    @Transactional(readOnly = true)
    public List<OriDataAnswer> findAnswersToSend() {
        List<OriDataAnswer> answerToSend = oriDataAnswerDao.findAnswersToSend(answerToSendAtOnce, tryLimit);
        return answerToSend;
    }
}
