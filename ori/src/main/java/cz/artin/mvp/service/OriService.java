package cz.artin.mvp.service;

import java.util.List;

import cz.artin.mvp.model.AnswerPageHolder;
import cz.artin.mvp.model.OriDataAnswer;

/**
 * Basic functionality services.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface OriService {

    /**
     * Creates answer in db.
     */
    void createAnswer(OriDataAnswer answer, String userId, Boolean isHelpful, String url);

    /**
     * Finds Answer from DB by id.
     * @param id of answer in DB
     * @return OriDataAnswer
     */
    OriDataAnswer findAnswer(Long id);

    /**
     * Updates Answer.
     * @param answer from DB
     * @param wouldCall sets wouldCall attribute, if it is not null
     * @param reason sets reason attribute, if it is not null
     * @param message sets message attribute, if it is not null
     * @param isForGuru sets guru attribute, if it is not null
     */
    void updateAnswer(OriDataAnswer answer, Boolean wouldCall,
                    String reason, String message, Boolean isForGuru);

    /**
     * Identifies if message should be sent to guru via email.
     * @param answer from DB
     */
    void sendToGuruIfNeeded(OriDataAnswer answer);

    /**
     * Read all answers filtered by given filter parameters. Newest records will be first.
     * @param page page number to display, if null 0 will be used
     * @param pageSize number of records on one page, if null 50 will be used
     * @param days day to the history to display, if null no limit will be applied
     * @param urlFilter regexp filter of url to display, can be null
     */
    List<OriDataAnswer> findAnswers(Integer page, Integer pageSize, Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly);

    /**
     * Count answers with given filter parameters.
     * @see OriDataAnswerDao#countAnswers(Integer, String, boolean, boolean)
     */
    int countAnswers(Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly);

    /**
     * Read answers for mailing to guru.
     * <ul>
     *         <li>Oldest answers are first in the list</li>
     *         <li>Number of answers is configured in <code>email.queue.at_once</code> parameters.</li>
     *         <li>Number of tries to send is configure in <code>email.queue.max_try</code> parameters.</li>
     * </ul>
     */
    List<OriDataAnswer> findAnswersToSend();

    /**
     * Count number of all answers.
     */
    Long countAllAnswers();

    /**
     * Validate params and eliminate some kinds of security risks.
     * <ul>
     *         <li>Section (urlFilter) parameter must be valid section name - invalid values will be set to "".</li>
     * </ul>
     * @param answerParams given command object
     */
    void validateParams(AnswerPageHolder answerParams);

    /**
     * Render xls file for export.
     */
    String renderExportTable(Iterable<OriDataAnswer> answers);
}
