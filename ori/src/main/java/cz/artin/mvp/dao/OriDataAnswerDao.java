package cz.artin.mvp.dao;

import java.util.List;

import cz.artin.mvp.model.OriDataAnswer;

/**
 * Dao for customer answers.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface OriDataAnswerDao {
    /**
     * Create new {@link OriDataAnswer}.
     */
    long create(OriDataAnswer answer);

    /**
     * Find certain {@link OriDataAnswer}.
     * @return found {@link OriDataAnswer} or null
     */
    OriDataAnswer findById(Long answerId);

    /**
     * Save given {@link OriDataAnswer}.
     */
    void save(OriDataAnswer answer);

    /**
     * Read all answers filtered by given filter parameters. Newest records will be first.
     * @param page page number to display
     * @param pageSize number of records on one page
     * @param days day to the history to display
     * @param urlFilter regexp filter of url to display
     * @param notHelpfulOnly if true only messages marked as non-helpful will be returned
     * @param messageOnly if true only messages with user reaction will be returned
     */
    List<OriDataAnswer> readAnswers(int page, int pageSize, int days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly);

    /**
     * Return count of all answers.
     */
    long count();

    /**
     * Read answers for mailing to guru.
     * @param answerToSendAtOnce number of answers (oldest first) to read, if 0 no limit will be applied
     * @param tryLimit how many times will try server to send mail, if 0 no limit will be applied
     */
    List<OriDataAnswer> findAnswersToSend(int answerToSendAtOnce, int tryLimit);

    /**
     * Flush transaction. Primarily for testing.
     */
    void flush();

    /**
     * Count answers with given filter parameters (see {@link #readAnswers(int, int, int, String, boolean, boolean)}).
     */
    int countAnswers(Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly);

}
