package cz.artin.mvp.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import cz.artin.mvp.model.OriDataAnswer;

/**
 * Represents hibernate implementation of DAO for customer answers.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Repository
public class OriDataAnswerDaoHibernate implements OriDataAnswerDao {
    @Autowired
    private SessionFactory sessionFactory;

    @Value("${export.url.faq_filter1}")
    private String faqUrl1;

    @Value("${export.url.faq_filter2}")
    private String faqUrl2;

    @Override
    public long create(OriDataAnswer answer) {
        return (Long) sessionFactory.getCurrentSession().save(answer);
    }

    @Override
    public OriDataAnswer findById(Long answerId) {
        Session session = sessionFactory.getCurrentSession();
        OriDataAnswer answer = (OriDataAnswer) session.createCriteria(OriDataAnswer.class)
                                .add(Restrictions.idEq(answerId))
                                .uniqueResult();
        return answer;
    }

    @Override
    public void save(OriDataAnswer answer) {
        sessionFactory.getCurrentSession().merge(answer);
    }

    @Override
    public List<OriDataAnswer> readAnswers(int page, int pageSize, int days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(OriDataAnswer.class);

        if (pageSize > 0) {
            crit.setMaxResults(pageSize);
            crit.setFirstResult(page * pageSize);
        }

        if (days > 0) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -days);
            Date startDate = cal.getTime();
            crit.add(Restrictions.ge("date", startDate));
        }

        if ("faq".equalsIgnoreCase(urlFilter)) {
            Criterion cr1 = Restrictions.like("url", faqUrl1);
            Criterion cr2 = Restrictions.like("url", faqUrl2);
            crit.add(Restrictions.or(cr1, cr2));
        }

        if (notHelpfulOnly) {
            crit.add(Restrictions.eq("helpful", Boolean.FALSE));
        }

        if (messageOnly) {
            crit.add(Restrictions.isNotNull("message"));
        }

        //order
        crit.addOrder(Order.desc("date"));

        @SuppressWarnings("unchecked")
        List<OriDataAnswer> result = crit.list();
        return result;
    }

    @Override
    public int countAnswers(Integer days, String urlFilter, boolean notHelpfulOnly, boolean messageOnly) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(OriDataAnswer.class).setProjection(Projections.rowCount());

        if ((days != null) && (days > 0)) {
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DATE, -days);
            Date startDate = cal.getTime();
            crit.add(Restrictions.ge("date", startDate));
        }

        if ("faq".equalsIgnoreCase(urlFilter)) {
            Criterion cr1 = Restrictions.like("url", faqUrl1);
            Criterion cr2 = Restrictions.like("url", faqUrl2);
            crit.add(Restrictions.or(cr1, cr2));
        }

        if (notHelpfulOnly) {
            crit.add(Restrictions.eq("helpful", Boolean.FALSE));
        }

        if (messageOnly) {
            crit.add(Restrictions.isNotNull("message"));
        }

        Long count = (Long) crit.uniqueResult();
        return count.intValue();
    }

    @Override
    public long count() {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(OriDataAnswer.class);

        crit.setProjection(Projections.rowCount()).uniqueResult();
        Long result = (Long) sessionFactory.getCurrentSession().getNamedQuery("answerCount").uniqueResult();

        return result;
    }

    @Override
    public List<OriDataAnswer> findAnswersToSend(int answerToSendAtOnce, int tryLimit) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(OriDataAnswer.class);

        if (answerToSendAtOnce > 0) {
            crit.setMaxResults(answerToSendAtOnce);
        }
        crit.addOrder(Order.desc("date"));
        crit.add(Restrictions.eq("guru", true));
        if (tryLimit > 0) {
            crit.add(Restrictions.le("tries", tryLimit));
        }
        crit.add(Restrictions.isNull("mailSentDate"));

        @SuppressWarnings("unchecked")
        List<OriDataAnswer> result = crit.list();
        return result;
    }

    @Override
    public void flush() {
        sessionFactory.getCurrentSession().flush();
    }

}
