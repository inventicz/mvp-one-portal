package cz.artin.mvp.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Index;

/**
 * Represents anser sent by end user from web pages.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Entity
//@Table(schema= "OP_ORI_USER", name = "ori_data_answer")
@Table(name = "ori_data_answer")
@NamedQueries(
        @NamedQuery(name = "answerCount", query = "select count(*) from OriDataAnswer")
)
public class OriDataAnswer implements Serializable {

    private static final long serialVersionUID = 1L;

    public static final int MESSAGE_LENGTH = 1024;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column(nullable = false, name = "datum")
    @Index(name = "ori_data_answer_date_ix")
    private Date date;

    @Column(length = 255, name = "url")
    @Index(name = "ori_data_answer_url_ix")
    private String url;

    @Column(length = 255, nullable = false, name = "user_id")
    private String userId;

    @Column(name = "is_helpful", columnDefinition = "BIT")
    private Boolean helpful;

    @Column(name = "would_call", columnDefinition = "BIT")
    private Boolean wouldCall;

    @Column(length = MESSAGE_LENGTH, name = "message")
    private String message;

    @Column(name = "guru", columnDefinition = "BIT")
    private Boolean guru;

    @Column(length = 255, name = "target_mail")
    private String targetMail;

    @Column(name = "mail_sent_date")
    private Date mailSentDate;

    @Column(name = "mail_last_try_date")
    private Date mailLastTryDate;

    @Column(name = "tries", nullable = false)
    private Integer tries = 0;

    @Column(length = 32, name = "reason")
    private String reason;

    //-------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getHelpful() {
        return helpful;
    }

    public void setHelpful(Boolean helpful) {
        this.helpful = helpful;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getWouldCall() {
        return wouldCall;
    }

    public void setWouldCall(Boolean wouldCall) {
        this.wouldCall = wouldCall;
    }

    public Boolean getGuru() {
        return guru;
    }

    public void setGuru(Boolean guru) {
        this.guru = guru;
    }

    public String getTargetMail() {
        return targetMail;
    }

    public void setTargetMail(String targetMail) {
        this.targetMail = targetMail;
    }

    public Date getMailSentDate() {
        return mailSentDate;
    }

    public void setMailSentDate(Date mailSentDate) {
        this.mailSentDate = mailSentDate;
    }

    public Date getMailLastTryDate() {
        return mailLastTryDate;
    }

    public void setMailLastTryDate(Date mailLastTryDate) {
        this.mailLastTryDate = mailLastTryDate;
    }

    public Integer getTries() {
        return tries;
    }

    public void setTries(Integer tries) {
        this.tries = tries;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append(this.id)
            .append(this.userId).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        OriDataAnswer other = (OriDataAnswer) obj;

        return new EqualsBuilder().append(this.getId(), other.getId()).isEquals();
    }
}
