package cz.artin.mvp.model;

import java.io.Serializable;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.PortletURL;

import com.liferay.portal.kernel.dao.search.DisplayTerms;
import com.liferay.portal.kernel.dao.search.SearchContainer;

/**
 * Represents data transfer object for holding answers together with metadata.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class AnswerPageHolder extends SearchContainer<OriDataAnswer> implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer days;

    private String urlFilter;

    private boolean showResults;

    private Boolean notHelpfulOnly;

    private Boolean messageOnly;

    /** Constructor. */
    public AnswerPageHolder() {
        super();
    }

    /** Constructor. */
    public AnswerPageHolder(PortletRequest portletRequest, DisplayTerms displayTerms, DisplayTerms searchTerms,
            String curParam, int cur, int delta, PortletURL iteratorUrl, List<String> headerNames,
            String emptyResultsMessage) {
        super(portletRequest, displayTerms, searchTerms, curParam, cur, delta, iteratorUrl, headerNames, emptyResultsMessage);
    }

    /** Constructor. */
    public AnswerPageHolder(PortletRequest portletRequest, DisplayTerms displayTerms, DisplayTerms searchTerms,
            String curParam, int delta, PortletURL iteratorUrl, List<String> headerNames, String emptyResultsMessage) {
        super(portletRequest, displayTerms, searchTerms, curParam, delta, iteratorUrl, headerNames, emptyResultsMessage);
    }

    /** Constructor. */
    public AnswerPageHolder(PortletRequest portletRequest, PortletURL iteratorUrl, List<String> headerNames,
            String emptyResultsMessage) {
        super(portletRequest, iteratorUrl, headerNames, emptyResultsMessage);
    }

    public Integer getDays() {
        return days;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public String getUrlFilter() {
        return urlFilter;
    }

    public void setUrlFilter(String urlFilter) {
        this.urlFilter = urlFilter;
    }

    public boolean isShowResults() {
        return showResults;
    }

    public void setShowResults(boolean showResults) {
        this.showResults = showResults;
    }

    public Boolean getNotHelpfulOnly() {
        return notHelpfulOnly;
    }

    public void setNotHelpfulOnly(boolean notHelpfulOnly) {
        this.notHelpfulOnly = notHelpfulOnly;
    }

    public Boolean getMessageOnly() {
        return messageOnly;
    }

    public void setMessageOnly(boolean messageOnly) {
        this.messageOnly = messageOnly;
    }

}
