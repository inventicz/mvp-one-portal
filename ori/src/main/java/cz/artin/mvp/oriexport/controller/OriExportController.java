package cz.artin.mvp.oriexport.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletMode;
import javax.portlet.PortletModeException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.model.AnswerPageHolder;
import cz.artin.mvp.model.OriDataAnswer;
import cz.artin.mvp.service.OriService;

/**
 * Represents controller for Export portlet.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Controller
@RequestMapping(value = "VIEW")
public class OriExportController {
    private static final String EXPORT_DAYS_ATTR = "exportDays";
    private static final String EXPORT_URL_FILTER_ATTR = "exportUrlFilter";
    private static final String EXPORT_NOT_HELPFUL_ONLY_ATTR = "exportNotHelpfulOnly";
    private static final String EXPORT_MESSAGE_ONLY_ATTR = "exportMessageOnly";

    private Log log = LogFactoryUtil.getLog(OriExportController.class.getName());

    @Autowired
    private OriService oriService;

    /**
     * When the portlet is added to the portal page this method will be called.
     * So this is the default render method
     */
    @RenderMapping
    public ModelAndView showStart(@ModelAttribute AnswerPageHolder search, @RequestParam(value = "cur", required = false) Integer cur, RenderRequest request, RenderResponse response) {
        WindowState windowState = request.getWindowState();

        final ModelAndView result;
        //display answers only in maximized state
        if (windowState.equals(WindowState.MAXIMIZED)) {
            result = showStartMaximized(search, cur, request, response);
        } else {
            int answerCount = oriService.countAllAnswers().intValue();
            AnswerPageHolder searchContainer = new AnswerPageHolder();
            searchContainer.setTotal(answerCount);

            result = new ModelAndView("exportMain");
            searchContainer.setShowResults(false);
            result.addObject("searchContainer", searchContainer);

        }
        return result;
    }

    private ModelAndView showStartMaximized(AnswerPageHolder search, Integer cur, RenderRequest request, RenderResponse response) {
        final int days;
        Integer searchDays = search.getDays();
        if (searchDays != null) {

            request.getPortletSession().setAttribute(EXPORT_DAYS_ATTR, searchDays);
            days = searchDays;
        } else {
            Integer sessionDays = (Integer) request.getPortletSession().getAttribute(EXPORT_DAYS_ATTR);
            if (sessionDays == null) {
                days = 30;
            } else {
                days = sessionDays;
            }
        }

        final String urlFilter;
        String searchUrlFilter = search.getUrlFilter();
        if (searchUrlFilter != null) {
            request.getPortletSession().setAttribute(EXPORT_URL_FILTER_ATTR, searchUrlFilter);
            urlFilter = searchUrlFilter;
        } else {
            String sessionUrlFilter = (String) request.getPortletSession().getAttribute(EXPORT_URL_FILTER_ATTR);
            if (sessionUrlFilter == null) {
                urlFilter = null;
            } else {
                urlFilter = sessionUrlFilter;
            }
        }

        final Boolean notHelpfulOnly;
        Boolean searchNotHelpfulOnly = search.getNotHelpfulOnly();
        if (searchNotHelpfulOnly != null) {
            request.getPortletSession().setAttribute(EXPORT_NOT_HELPFUL_ONLY_ATTR, searchNotHelpfulOnly);
            notHelpfulOnly = searchNotHelpfulOnly;
        } else {
            Boolean sessionNotHelpfulOnly = (Boolean) request.getPortletSession().getAttribute(EXPORT_NOT_HELPFUL_ONLY_ATTR);
            if (sessionNotHelpfulOnly == null) {
                notHelpfulOnly = true;
            } else {
                notHelpfulOnly = sessionNotHelpfulOnly;
            }
        }

        final Boolean messageOnly;
        Boolean searchMessageOnly = search.getMessageOnly();
        if (searchMessageOnly != null) {
            request.getPortletSession().setAttribute(EXPORT_MESSAGE_ONLY_ATTR, searchMessageOnly);
            messageOnly = searchMessageOnly;
        } else {
            Boolean sessionMessageOnly = (Boolean) request.getPortletSession().getAttribute(EXPORT_MESSAGE_ONLY_ATTR);
            if (sessionMessageOnly == null) {
                messageOnly = true;
            } else {
                messageOnly = sessionMessageOnly;
            }
        }

        final AnswerPageHolder searchContainer = new AnswerPageHolder(request, response.createRenderURL(), null, "Žádné záznamy nenalezeny.");
        searchContainer.setMessageOnly(messageOnly);
        searchContainer.setNotHelpfulOnly(notHelpfulOnly);
        searchContainer.setDays(days);
        searchContainer.setUrlFilter(urlFilter);
        searchContainer.setDelta(200);

        searchContainer.setTotal(oriService.countAnswers(days, urlFilter, notHelpfulOnly, messageOnly));

        int curToUse = 0;
        if (cur != null) {
            curToUse = cur - 1;
        }
        List<OriDataAnswer> answers = oriService.findAnswers(curToUse, 200, days, urlFilter, notHelpfulOnly, messageOnly);
        searchContainer.setResults(answers);

        searchContainer.setShowResults(true);
        ModelAndView result = new ModelAndView("exportMain");
        result.addObject("searchContainer", searchContainer);
        return result;
    }

    /**
     * Handle full display page (portlet will be maximized).
     */
    @ActionMapping(value = "openExport")
    public void handleOpenExport(@ModelAttribute AnswerPageHolder answers,
            ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws PortletModeException, WindowStateException {
        //actionResponse.setRenderParameter("action", "export");
        model.addAttribute("successModel", answers);

        actionResponse.setPortletMode(PortletMode.VIEW);
        actionResponse.setWindowState(WindowState.MAXIMIZED);
    }

    /**
     * Handle download of xls export.
     */
    @ResourceMapping("fileDownload")
    public void serveResource(@RequestParam("days") Integer days, @RequestParam("urlFilter") String urlFilter,
            ResourceRequest request, ResourceResponse response) throws IOException {
        log.info("Exporting report for " + days + "days");

        final int daysChecked;
        if ((days == null) || (days < 1) || (days > 30)) {
            log.info("Limit for days is 30.");
            daysChecked = 30;
        } else {
            daysChecked = days;
        }

        List<OriDataAnswer> answers = oriService.findAnswers(0, -1, daysChecked, urlFilter, true, true);

        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.addProperty("Content-disposition", "atachment; filename=export.xls");

        String render = oriService.renderExportTable(answers);
        byte[] bytes = render.getBytes();

        OutputStream out = response.getPortletOutputStream();
        out.write(bytes);
        out.flush();
        out.close();
    }
}
