<%@page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />

<portlet:actionURL var="submitFormURL" name="handleFirstAnswer"/>

<div class="page-width page-width--edge page-indent h-text-center" id="oriform" >
	<div class="just-box just-box--inline">
		Pomohla vám tato stránka?
			<form:form class="ori-start-form" name="customer" method="post" action="${submitFormURL.toString()}#oriform">
				<button name="isHelpful" value="true" class="button button--blue" type="submit">Ano</button>
				<button name="isHelpful" value="false" class="button button--blue just-box__left-indent" type="submit">Ne</button>
			</form:form>
		<div class="clear"></div>
	</div>
</div>
