<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />

<portlet:actionURL var="submitFormURL" name="handleWouldCall"/>

<div class="page-width page-width--edge page-indent h-text-center" id="oriform">
	<div class="just-box just-box--inline">
		Kdyby Vám informace nepomohly, volali byste na naší zákazníckou linku?
			<form:form class="ori-start-form" name="customer" method="post" action="<%=submitFormURL.toString() + \"#oriform\" %>">
				<input name="answerId" value="${answerId}" type="hidden"/>
				<input name="isHelpful" value="true" type="hidden">
				<button name="wouldCall" value="true" class="button button--blue" type="submit">Ano</button>
				<button name="wouldCall" value="false" class="button button--blue" type="submit">Ne</button>
			</form:form>
		<div class="clear"></div>
	</div>
</div>
