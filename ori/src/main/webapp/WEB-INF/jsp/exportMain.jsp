<%@page import="javax.portlet.PortletURL"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<%@ page import="org.apache.commons.lang.StringEscapeUtils"%>
<portlet:actionURL var="submitFormURL" name="openExport" />
<portlet:resourceURL var="fileDownloadURL" id="fileDownload" />
<portlet:defineObjects />
<%
PortletURL portletURL = renderResponse.createActionURL();
%>

<script type="text/javascript">
function downloadLink() {
	var link = "<%=fileDownloadURL.toString()%>&days=";
	link += document.getElementById("ori-export-days").value;
	link += "&urlFilter=";
	link += document.getElementById("ori-export-section").value;
	console.log("link=" + link);
	return link;
}
</script>

<div class="ori-export">
<c:choose>
	<c:when test="${searchContainer.showResults}">
		<form:form id="ori-export-form" name="export" method="post" modelAttribute="searchContainer" action="<%=submitFormURL.toString() %>">
			<form:select id="ori-export-days" path="days" onchange="this.form.submit();">
				<form:option value="-1"><liferay-ui:message key="ori.stats.time.forever" /></form:option>
				<form:option value="7"><liferay-ui:message key="ori.stats.time.week" /></form:option>
				<form:option value="30"><liferay-ui:message key="ori.stats.time.month" /></form:option>
			</form:select>
			<form:select id="ori-export-section" path="urlFilter" onchange="this.form.submit();">
				<form:option value=""><liferay-ui:message key="ori.stats.section.value.all" /></form:option>
				<form:option value="faq"><liferay-ui:message key="ori.stats.section.value.faq" /></form:option>
			</form:select>
			<form:select id="ori-export-helpful" path="notHelpfulOnly" onchange="this.form.submit();">
				<form:option value="true"><liferay-ui:message key="ori.stats.helpful.value.not" /></form:option>
				<form:option value="false"><liferay-ui:message key="ori.stats.helpful.value.all" /></form:option>
			</form:select>
			<form:select id="ori-export-message-filter" path="messageOnly" onchange="this.form.submit();">
				<form:option value="true"><liferay-ui:message key="ori.stats.message.value.true" /></form:option>
				<form:option value="false"><liferay-ui:message key="ori.stats.message.value.all" /></form:option>
			</form:select>
			<a href="#" onClick="window.location=downloadLink();">Download</a>
		</form:form>

		<liferay-ui:search-container searchContainer="${searchContainer}" iteratorURL="<%= portletURL %>" deltaConfigurable="false">

			<liferay-ui:search-container-results results="${searchContainer.results}" total="${searchContainer.total}"/>

			<liferay-ui:search-container-row className="cz.artin.mvp.model.OriDataAnswer" keyProperty="id" modelVar="answer">
				<liferay-ui:search-container-column-text cssClass="ori-export-table-id" name="id" value="<%= answer.getId().toString() %>"	/>
				<liferay-ui:search-container-column-date cssClass="ori-export-table-date" name="Datum" value="<%= answer.getDate() %>" />
				<liferay-ui:search-container-column-text cssClass="ori-export-table-url" name="URL" value="<%= answer.getUrl() %>"	/>
				<liferay-ui:search-container-column-text  cssClass="ori-export-table-message" name="Zpráva"
					value="<%= (answer.getMessage() == null) ? \"\" : StringEscapeUtils.escapeHtml(answer.getMessage()) %>"	/>
				<liferay-ui:search-container-column-text  cssClass="ori-export-table-boolean" name="Guru" value="<%= (answer.getGuru() != null) && answer.getGuru() ? \"X\" : \"\"%>" />
				<c:if test="${not searchContainer.notHelpfulOnly}">
					<liferay-ui:search-container-column-text cssClass="ori-export-table-boolean" name="Užitečné" value="<%= (answer.getHelpful() != null) && answer.getHelpful() ? \"X\" : \"\"%>" />
					<liferay-ui:search-container-column-text cssClass="ori-export-table-boolean" name="Volali byste" value="<%= (answer.getWouldCall() != null) && answer.getWouldCall() ? \"X\" : \"\"%>" />
				</c:if>
			</liferay-ui:search-container-row>

			<liferay-ui:search-iterator searchContainer="${searchContainer}"/>

		</liferay-ui:search-container>
	</c:when>
	<c:otherwise>
		<div class="page-width page-width--edge page-indent h-text-center">
			<div class="just-box just-box--inline">
				<div><liferay-ui:message key="ori.answers.count" />: ${searchContainer.total}</div>
				<form:form name="export" method="post" action="<%=submitFormURL.toString() %>">
					<button class="button button--blue" type="submit"><liferay-ui:message key="ori.stats" /></button>
				</form:form>
			</div>
		</div>
	</c:otherwise>
</c:choose>
</div>
