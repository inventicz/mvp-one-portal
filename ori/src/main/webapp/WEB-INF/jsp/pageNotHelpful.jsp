<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleNotHelpfulForm"/>

<script type="text/javascript">
	function oriShowMessage() {
		document.getElementById('submitButton').disabled = false;
		document.getElementById('message').style.display = 'block';
	}
	function showChat(chatUrl) {
		var form = document.getElementById("oriform_form");
		form.submit();
	}
</script>
<div class="ori-reason-form page-width page-width--edge page-indent h-text-center" id="oriform">
	<div class="just-box just-box--inline">
		Co bychom mohli podle Vás zlepšit?

		<form:form name="answer" method="post" modelAttribute="answer" action="<%=submitFormURL.toString() + \"#oriform\" %>" id="oriform_form">
			<form:input path="id" type="hidden"></form:input>
				<div class="ori-reason"><form:radiobutton path="reason" value="neprehledne" label="Stránka je nepřehledná" onclick="oriShowMessage()"/></div>
				<div class="ori-reason"><form:radiobutton path="reason" value="slozite" label="Stránka je složitá" onclick="oriShowMessage()"/></div>
				<div class="ori-reason"><form:radiobutton path="reason" value="nenasel" label="Nenašel jsem, co hledám" onclick="oriShowMessage()"/></div>
				<div class="ori-reason"><form:radiobutton path="reason" value="pomoc" label="Potřebuji pomoc" onclick="showChat('${chatUrl}')"/></div>

				<div style="display:none;" id=message>
					Napište nám prosím:
					<form:textarea path="message" maxlength="1024" />
					<div class="clear"></div>
					<button disabled class="button button--blue" id="submitButton" type="submit">Odeslat</button>
				</div>
		</form:form>
	</div>
</div>
