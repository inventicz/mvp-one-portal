<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="page-width page-width--edge page-indent h-text-center" id="oriform">
	<div class="just-box just-box--inline">
		Děkujeme
	</div>
</div>

<c:if test="${showChat}">
	<script type="text/javascript">
		window.location.replace("${chatUrl}");
	</script>
</c:if>