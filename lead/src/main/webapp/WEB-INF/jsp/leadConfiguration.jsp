<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleConfigurationForm"/>

<div>
	<div>
		<form name="configuration" method="post" action="<%=submitFormURL.toString() %>">
			<button id="submitButton" type="submit">Načíst / vytvořit konfiguraci</button>
		</form>
	</div>
</div>
