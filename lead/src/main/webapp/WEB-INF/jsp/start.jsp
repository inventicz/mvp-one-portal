<%@ page contentType="text/html; charset=UTF-8" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<portlet:defineObjects />
<portlet:actionURL var="submitFormURL" name="handleBusinessLeadForm"/>

<form:form name="businessLead" method="post" modelAttribute="businessLead" action="<%=submitFormURL.toString() %>">
	<div class="contact-us-box__item__text-input"><form:input path="businessId" class="form__input" placeholder="Vaše IČ" /></div>
	<form:errors path="businessIdErrorHolder" />
	<div class="contact-us-box__item__text-input"><form:input path="phone" class="form__input" placeholder="Vaše telefonní číslo" /></div>
	<form:errors path="phoneErrorHolder" />
	<div style="display:none"><form:input path="text" /></div>
	<div class="contact-us-box__item__text-input"><button type="submit" class="button button--blue">Ozvěte se mi</button></div>
</form:form>
