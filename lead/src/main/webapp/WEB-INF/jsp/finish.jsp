<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects />

<portlet:actionURL var="endURL" name="end"/>

<script type="text/javascript">
$(function() {
	$.magnificPopup.open({
		items: {
		    src: '<div class="lead-popup">${message}</div>',
		    type: 'inline'
		},
		callbacks: {
			close: function() {
		    	window.location.replace("${endURL}");
			}
		},
			closeBtnInside: true
		});

});
</script>
