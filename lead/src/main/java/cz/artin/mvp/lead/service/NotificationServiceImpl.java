package cz.artin.mvp.lead.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.lead.model.BusinessLead;

/**
 * Represents high level services for mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
@Service("notificationService")
public class NotificationServiceImpl implements NotificationService {

    private Log log = LogFactoryUtil.getLog(NotificationServiceImpl.class.getName());

    @Autowired
    private LeadService leadService;

    @Autowired
    private MailSenderService mailSenderService;

    @Transactional
    @Override
    @Scheduled(fixedRate = 60000)
    public void sendEmails() {
        log.debug("Sending notification mails");
        List<BusinessLead> leadsToSend = leadService.findLeadsToSend();

        for (BusinessLead lead : leadsToSend) {
            sendEmail(lead);
        }
    }

    /**
     * Forward single {@link OriDataAnswer} to the O2 mail.
     * @return {@link OriDataAnswer} updated regarding result of the sending
     */
    private BusinessLead sendEmail(BusinessLead lead) {
        log.info("Sending notification mail to o2");

        boolean messageError = false;
        try {
            String mailSubject = "​WEB Poptávka expresní ­ IC: " + lead.getBusinessId() + " , tel. cislo: " + lead.getPhone();

            mailSenderService.sendMail(mailSubject, lead);
        } catch (MailException e) {
            messageError = true;
            log.warn("Mail " + lead + " was not sent.", e);
            lead.setTries(lead.getTries() + 1);
            lead.setMailLastTryDate(new Date());
        }

        if (!messageError) {
            Date now = new Date();
            if (log.isDebugEnabled()) {
                log.debug("Mail " + lead.getId() + " sent " + now);
            }
            lead.setMailSentDate(now);
        }

        return lead;
    }

    public void setMailSenderService(MailSenderService mailSenderService) {
        this.mailSenderService = mailSenderService;
    }
}
