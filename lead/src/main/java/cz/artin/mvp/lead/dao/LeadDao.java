package cz.artin.mvp.lead.dao;

import java.util.List;

import cz.artin.mvp.lead.model.BusinessLead;

/**
 * Dao for main project class.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface LeadDao {
    /**
     * Create new {@link Lead}.
     */
    long create(BusinessLead lead);

    /**
     * Read answers for mailing to O2.
     * @param leadsToSendAtOnce number of leads (oldest first) to read, if 0 no limit will be applied
     * @param tryLimit how many times will try server to send mail, if 0 no limit will be applied
     */
    List<BusinessLead> findLeadsToSend(int leadsToSendAtOnce, int tryLimit);
}
