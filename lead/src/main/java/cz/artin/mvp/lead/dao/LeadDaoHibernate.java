package cz.artin.mvp.lead.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import cz.artin.mvp.lead.model.BusinessLead;

/**
 * Represents DAO for leads.
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Repository
public class LeadDaoHibernate implements LeadDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public long create(BusinessLead lead) {
        return (Long) sessionFactory.getCurrentSession().save(lead);
    }

    @Override
    public List<BusinessLead> findLeadsToSend(int leadsToSendAtOnce, int tryLimit) {
        Criteria crit = sessionFactory.getCurrentSession().createCriteria(BusinessLead.class);

        if (leadsToSendAtOnce > 0) {
            crit.setMaxResults(leadsToSendAtOnce);
        }
        crit.addOrder(Order.desc("date"));
        if (tryLimit > 0) {
            crit.add(Restrictions.le("tries", tryLimit));
        }
        crit.add(Restrictions.isNull("mailSentDate"));

        @SuppressWarnings("unchecked")
        List<BusinessLead> result = crit.list();
        return result;
    }

}
