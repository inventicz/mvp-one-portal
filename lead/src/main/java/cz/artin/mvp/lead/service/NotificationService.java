package cz.artin.mvp.lead.service;

/**
 * High level services for mails handling.
 *
 * @author Ján Nosko [jan.nosko@artin.cz]
 *
 */
public interface NotificationService {

    /**
     * Send emails to O2. Usually called by scheduler.
     */
    void sendEmails();
}
