package cz.artin.mvp.lead.model;

import java.io.Serializable;

import cz.artin.mvp.lead.model.validation.BussinessId;
import cz.artin.mvp.lead.model.validation.PhoneNumber;

/**
 * Represents lead DTO.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class BusinessLeadDto implements Serializable {

    private static final long serialVersionUID = 1L;

    @BussinessId
    private String businessId;

    @PhoneNumber
    private String phone;

    private String businessIdErrorHolder;

    private String phoneErrorHolder;

    /** Used for robot detection. */
    private String text;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public String getBusinessIdErrorHolder() {
        return businessIdErrorHolder;
    }

    public void setBusinessIdErrorHolder(String businessIdErrorHolder) {
        this.businessIdErrorHolder = businessIdErrorHolder;
    }

    public String getPhoneErrorHolder() {
        return phoneErrorHolder;
    }

    public void setPhoneErrorHolder(String phoneErrorHolder) {
        this.phoneErrorHolder = phoneErrorHolder;
    }

    @Override
    public String toString() {
        return "BusinessLeadDto [businessId=" + businessId + ", phone=" + phone + "]";
    }
}
