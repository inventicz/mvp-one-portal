package cz.artin.mvp.lead.service;

import java.util.List;

import cz.artin.mvp.lead.model.BusinessLead;
import cz.artin.mvp.lead.model.LeadConfiguration;
import cz.artin.mvp.lead.model.BusinessLeadDto;

/**
 * Lead related service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface LeadService {

    /**
     * Create lead.
     */
    BusinessLead createLead(BusinessLeadDto leadDto, LeadConfiguration configuration, String url);

    /**
     * Find leads needs to be sent to O2.
     */
    List<BusinessLead> findLeadsToSend();
}
