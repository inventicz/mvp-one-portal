package cz.artin.mvp.lead.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Represents validator for CZ Bussiness ID.
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class BusinessIdValidator implements ConstraintValidator<BussinessId, String> {

    @Override
    public void initialize(BussinessId constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        final boolean result;
        if (value != null) {
            String phoneNumber = value.replaceAll("\\s", "");
            result = phoneNumber.matches("[0-9]{8}");
        } else {
            result = false;
        }

        return result;
    }
}
