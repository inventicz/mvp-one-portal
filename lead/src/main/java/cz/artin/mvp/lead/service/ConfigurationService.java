package cz.artin.mvp.lead.service;

import javax.portlet.PortletRequest;

import org.joda.time.DateTimeZone;
import org.joda.time.LocalDateTime;

import cz.artin.mvp.lead.model.LeadConfiguration;

/**
 * Service for manipulating configuration.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public interface ConfigurationService {
    /**
     * Reread configuration from the DDL. If there is no configuration DDL in the system yet create default one.
     */
    LeadConfiguration initConfiguration(PortletRequest request);

    /**
     * Get already initialized configuration. Can be null.
     */
    LeadConfiguration getConfiguration();

    /**
     * Get configuration. Configuration will be read/created if initialization is needed.
     */
    LeadConfiguration getConfiguration(PortletRequest request);

    /**
     * Resolve message to display based on date/time.
     */
    String getResponseTextByDate(LocalDateTime date);

    /**
     * Resolve latest possible time to answer customer.
     */
    LocalDateTime getLatestReply(LocalDateTime date);

    /**
     * Get timezone of portal.
     * @return
     */
    DateTimeZone getTimeZone();
}
