package cz.artin.mvp.lead.service;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailPreparationException;
import org.springframework.mail.MailSendException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.util.mail.MailEngine;
import com.liferay.util.mail.MailEngineException;

import cz.artin.mvp.lead.model.BusinessLead;

/**
 * Represents mail queue handler. Sends mails to o2 address.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
@Service
public class MailSenderServiceImpl implements MailSenderService {
    private Log log = LogFactoryUtil.getLog(MailSenderServiceImpl.class.getName());

    @Value("#{T(org.apache.commons.io.IOUtils).toString("
                + "T(org.springframework.util.ResourceUtils).getURL("
                    + "'${email.template.path}'))}")
    private String template;

    @Value("${email.template.date_format}")
    private String dateFormatDef;

    @Autowired
    private ConfigurationService configurationService;

    private DateFormat dateFormat;

    @PostConstruct
    public void init() {
        this.dateFormat = new SimpleDateFormat(dateFormatDef);
    }

    @Override
    public void sendMail(String subject, BusinessLead lead) {
        if (!StringUtils.hasText(subject)) {
            throw new MailParseException("Subject cannot be empty");
        }
        if (lead == null) {
            throw new MailParseException("Answer is null.");
        }

        String mailBody = renderMailTemplate(lead);

        MailMessage mailMessage = new MailMessage();
        mailMessage.setHTMLFormat(false);
        mailMessage.setSubject(subject);
        mailMessage.setBody(mailBody);

        try {
            mailMessage.setFrom(new InternetAddress(configurationService.getConfiguration().getMailFrom(), "Business Web"));
            mailMessage.setTo(new InternetAddress(configurationService.getConfiguration().getMailTo()));
            log.debug("Sending mail to o2");
            MailEngine.send(mailMessage);
        } catch (UnsupportedEncodingException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (AddressException e) {
            throw new MailPreparationException("Problem sending mail", e);
        } catch (MailEngineException e) {
            throw new MailSendException("Problem sending mail", e);
        }

    }

    /**
     * Render template - replace placeholders in the template by given values.
     * <ul>
     *         <li>$$BODY$$</li>
     *         <li>$$REASON$$</li>
     *         <li>$$DATE$$</li>
     * </ul>
     */
    protected String renderMailTemplate(BusinessLead lead) {
        Date replyDate = configurationService.getLatestReply(new LocalDateTime(lead.getDate().getTime())).toDate();

        String result = template.replaceAll("\\$\\$businessId\\$\\$", lead.getBusinessId())
                .replaceAll("\\$\\$phone\\$\\$", lead.getPhone())
                .replaceAll("\\$\\$url\\$\\$", lead.getUrl())
                .replaceAll("\\$\\$date\\$\\$", dateFormat.format(lead.getDate()))
                .replaceAll("\\$\\$replyDate\\$\\$", dateFormat.format(replyDate));
        return result;
    }

    protected String getTemplate() {
        return template;
    }
}
