package cz.artin.mvp.lead.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.lead.model.LeadConfiguration;
import cz.artin.mvp.lead.service.ConfigurationService;

/**
 * Represents controller for Lead portlet.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Controller
@RequestMapping(value = "EDIT")
public class LeadPreferencesController {
    private Log log = LogFactoryUtil.getLog(LeadPreferencesController.class);

    @Autowired
    private ConfigurationService configurationService;

    /**
     * Default render method.
     */
    @RenderMapping
    public ModelAndView showConfiguration(RenderRequest request, RenderResponse response) {
        final ModelAndView result = new ModelAndView("leadConfiguration");

        return result;
    }

    /** Handle configuration save. */
    @ActionMapping(value = "handleConfigurationForm")
    public void handleConfigurationForm(@ModelAttribute LeadConfiguration configuration, ActionRequest actionRequest,
            ActionResponse actionResponse) {
        log.debug("Handling configuration form");

        configurationService.initConfiguration(actionRequest);
    }

}
