package cz.artin.mvp.lead.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.hibernate.annotations.Index;

/**
 * Represents lead from business web.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Entity
@Table(schema = "OP_LEAD_USER", name = "lead_data")
public class BusinessLead implements Serializable {

    private static final long serialVersionUID = 1L;

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column(nullable = false, name = "datum")
    @Index(name = "ori_data_lead_date_ix")
    private Date date;

    @Column(nullable = false, length = 255, name = "url")
    @Index(name = "ori_data_lead_url_ix")
    private String url;

    @Column(length = 32, nullable = false, name = "business_id")
    private String businessId;

    @Column(length = 32, nullable = false, name = "phone")
    private String phone;

    @Column(nullable = false, length = 255, name = "target_mail")
    private String targetMail;

    @Column(name = "mail_sent_date")
    private Date mailSentDate;

    @Column(name = "mail_last_try_date")
    private Date mailLastTryDate;

    @Column(name = "tries", nullable = false)
    private Integer tries = 0;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTargetMail() {
        return targetMail;
    }

    public void setTargetMail(String targetMail) {
        this.targetMail = targetMail;
    }

    public Date getMailSentDate() {
        return mailSentDate;
    }

    public void setMailSentDate(Date mailSentDate) {
        this.mailSentDate = mailSentDate;
    }

    public Date getMailLastTryDate() {
        return mailLastTryDate;
    }

    public void setMailLastTryDate(Date mailLastTryDate) {
        this.mailLastTryDate = mailLastTryDate;
    }

    public Integer getTries() {
        return tries;
    }

    public void setTries(Integer tries) {
        this.tries = tries;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
            .append(this.id)
            .append(this.businessId)
            .append(this.phone).toString();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(this.id).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BusinessLead other = (BusinessLead) obj;

        return new EqualsBuilder().append(this.getId(), other.getId()).isEquals();
    }
}
