package cz.artin.mvp.lead.model;

import java.io.Serializable;
import java.util.Set;

/**
 *    Represents holder for configuration values. Values are read from DDL.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class LeadConfiguration implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mailFrom;

    private String mailTo;

    private String workingHoursText;

    private String nextDayText;

    private String todayText;

    private Set<DayOfYear> holidays;

    private String businessIdErrorText;

    private String phoneErrorText;

    private String messageErrorText;

    public Set<DayOfYear> getHolidays() {
        return holidays;
    }

    public void setHolidays(Set<DayOfYear> holidays) {
        this.holidays = holidays;
    }

    public String getWorkingHoursText() {
        return workingHoursText;
    }

    public void setWorkingHoursText(String workingHoursText) {
        this.workingHoursText = workingHoursText;
    }

    public String getNextDayText() {
        return nextDayText;
    }

    public void setNextDayText(String nextDayText) {
        this.nextDayText = nextDayText;
    }

    public String getTodayText() {
        return todayText;
    }

    public void setTodayText(String todayText) {
        this.todayText = todayText;
    }

    public String getMailTo() {
        return mailTo;
    }

    public void setMailTo(String mailTo) {
        this.mailTo = mailTo;
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public void setMailFrom(String targetMail) {
        this.mailFrom = targetMail;
    }

    public String getBusinessIdErrorText() {
        return businessIdErrorText;
    }

    public void setBusinessIdErrorText(String businessIdErrorText) {
        this.businessIdErrorText = businessIdErrorText;
    }

    public String getPhoneErrorText() {
        return phoneErrorText;
    }

    public void setPhoneErrorText(String phoneErrorText) {
        this.phoneErrorText = phoneErrorText;
    }

    public String getMessageErrorText() {
        return messageErrorText;
    }

    public void setMessageErrorText(String messageErrorText) {
        this.messageErrorText = messageErrorText;
    }
}
