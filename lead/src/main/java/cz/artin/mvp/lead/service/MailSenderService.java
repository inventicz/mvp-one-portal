package cz.artin.mvp.lead.service;

import org.springframework.mail.MailException;

import cz.artin.mvp.lead.model.BusinessLead;

/**
 * Mail queue handler. Sends mails to o2 address.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 */
public interface MailSenderService {

    /**
     * Render and send mail to o2.
     * @param lead message model
     * @throws MailException when mail sending was not successful
     */
    void sendMail(String subject, BusinessLead lead) throws MailException;
}
