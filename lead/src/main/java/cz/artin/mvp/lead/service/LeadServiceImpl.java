package cz.artin.mvp.lead.service;

import java.util.Date;
import java.util.List;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import cz.artin.mvp.lead.dao.LeadDao;
import cz.artin.mvp.lead.model.BusinessLead;
import cz.artin.mvp.lead.model.BusinessLeadDto;
import cz.artin.mvp.lead.model.LeadConfiguration;

/**
 * Represents Lead related service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Service
public class LeadServiceImpl implements LeadService {
    private Log log = LogFactoryUtil.getLog(LeadServiceImpl.class);

    /** How many times will try server to send mail. */
    @Value("${email.queue.max_try}")
    private int tryLimit;

    /** How many mail will be send in one cycle. */
    @Value("${email.queue.at_once}")
    private int leadsToSendAtOnce;

    @Autowired
    private LeadDao leadDao;

    @Autowired
    private ConfigurationService configurationService;

    @Override
    @Transactional
    public BusinessLead createLead(BusinessLeadDto leadDto, LeadConfiguration configuration, String url) {
        log.info("Creating new lead: " + leadDto);
        Date now = new LocalDateTime(configurationService.getTimeZone()).toDate();

        BusinessLead lead = new BusinessLead();
        lead.setBusinessId(leadDto.getBusinessId().replaceAll("\\s", ""));
        lead.setPhone(leadDto.getPhone().replaceAll("\\s", ""));
        lead.setDate(now);
        lead.setTargetMail(configuration.getMailTo());
        lead.setUrl(url);

        leadDao.create(lead);

        return lead;
    }

    @Override
    public List<BusinessLead> findLeadsToSend() {
        List<BusinessLead> findLeadsToSend = leadDao.findLeadsToSend(leadsToSendAtOnce, tryLimit);
        return findLeadsToSend;
    }
}
