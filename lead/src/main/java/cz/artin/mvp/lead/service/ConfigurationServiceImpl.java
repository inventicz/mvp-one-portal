package cz.artin.mvp.lead.service;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.PortletRequest;

import org.joda.time.DateTimeConstants;
import org.joda.time.DateTimeZone;
import org.joda.time.Duration;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portlet.dynamicdatalists.model.DDLRecord;
import com.liferay.portlet.dynamicdatalists.model.DDLRecordSet;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordSetLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.storage.Field;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;

import cz.artin.mvp.lead.model.DayOfYear;
import cz.artin.mvp.lead.model.LeadConfiguration;
import cz.artin.mvp.lead.util.LiferayDdlUtil;

/**
 * Represents configuration related service.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Service
public class ConfigurationServiceImpl implements ConfigurationService {
    private Log log = LogFactoryUtil.getLog(ConfigurationServiceImpl.class);

    /** Will be used when configuration value is not set. */
    @Value("${ddl.mail_to.default}")
    private String defaultEmailAddressTo;

    /** Will be used when configuration value is not set. */
    @Value("${ddl.mail_from.default}")
    private String defaultEmailAddressFrom;

    @Value("${ddl.recordset.name}")
    private String configurationRecordSetName;

    @Value("${ddl.mail_to.key}")
    private String mailToKey;

    @Value("${ddl.mail_from.key}")
    private String mailFromKey;

    @Value("${ddl.working_hours_text.key}")
    private String workingHoursTextKey;

    @Value("${ddl.working_hours_text.default}")
    private String defaultWorkingHoursText;

    @Value("${ddl.next_day_text.key}")
    private String nextDayTextKey;

    @Value("${ddl.next_day_text.default}")
    private String defaultNextDayText;

    @Value("${ddl.today_text.key}")
    private String todayTextKey;

    @Value("${ddl.today_text.default}")
    private String defaultTodayText;

    @Value("${ddl.holidays.key}")
    private String holidaysKey;

    @Value("${ddl.holidays.default}")
    private String defaultHolidaysRaw;

    @Value("${ddl.business_id_error_text.key}")
    private String businessIdErrorTextKey;

    @Value("${ddl.business_id_error_text.default}")
    private String defaultBusinessIdErrorText;

    @Value("${ddl.phone_error_text.key}")
    private String phoneErrorTextKey;

    @Value("${ddl.phone_error_text.default}")
    private String defaultPhoneErrorText;

    @Value("${ddl.message_error_text.key}")
    private String messageErrorTextKey;

    @Value("${ddl.message_error_text.default}")
    private String defaultMessageErrorText;

    @Value("#{T(org.apache.commons.io.IOUtils).toString("
            + "T(org.springframework.util.ResourceUtils).getURL("
                + "'${ddl.xsd.path}'))}")
    private String configurationDdlXsd;

    @Value("${global.timezone:Europe/Prague}")
    private String timeZoneName;

    private Pattern datePattern = Pattern.compile("([1-9]|[12]\\d|3[01])\\.([1-9]|1[012])\\.");

    private LeadConfiguration configuration;

    @Override
    public LeadConfiguration initConfiguration(PortletRequest request) {
        readConfigurationDdl(request);

        return this.configuration;
    }

    @Override
    public LeadConfiguration getConfiguration() {
        return this.configuration;
    }

    @Override
    public LeadConfiguration getConfiguration(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("getConfiguration - request=" + request);
        }
        if (configuration == null) {
            if (log.isDebugEnabled()) {
                log.debug("getConfiguration - configuration not initialized");
            }
            readConfigurationDdl(request);
        }
        return this.configuration;
    }


    private void readConfigurationDdl(PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - request=" + request);
        }
        DDLRecordSet configRecordSet = this.getDdlRecordSetByName(configurationRecordSetName);
        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - configRecordSet=" + configRecordSet);
        }
        if (configRecordSet == null) {
            configRecordSet = createConfigurationRecordset(request);
            if (log.isDebugEnabled()) {
                log.debug("readConfigurationDdl - created configRecordSet=" + configRecordSet);
            }

            storeDefaultValuesToDdlRecord(configRecordSet, request);
        }

        if (log.isDebugEnabled()) {
            log.debug("readConfigurationDdl - going to read configuration");
        }
        this.configuration = readConfiguration(configRecordSet, request);
    }

    private void storeDefaultValuesToDdlRecord(DDLRecordSet configRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("storeDefaultValuesToDdlRecord - configRecordSet=" + configRecordSet);
        }

        Fields fields = new Fields();

        fields.put(createField(mailToKey, defaultEmailAddressTo));
        fields.put(createField(mailFromKey, defaultEmailAddressFrom));
        fields.put(createField(workingHoursTextKey, defaultWorkingHoursText));
        fields.put(createField(nextDayTextKey, defaultNextDayText));
        fields.put(createField(todayTextKey, defaultTodayText));
        fields.put(createField(holidaysKey, defaultHolidaysRaw));
        fields.put(createField(businessIdErrorTextKey, defaultBusinessIdErrorText));
        fields.put(createField(phoneErrorTextKey, defaultPhoneErrorText));
        fields.put(createField(messageErrorTextKey, defaultPhoneErrorText));

        if (log.isDebugEnabled()) {
            log.debug("storeDefaultValuesToDdlRecord - default values");
            log.debug(mailToKey + "=" + defaultEmailAddressTo);
            log.debug(mailFromKey + "=" + defaultEmailAddressFrom);
            log.debug(workingHoursTextKey + "=" + defaultWorkingHoursText);
            log.debug(nextDayTextKey + "=" + defaultNextDayText);
            log.debug(todayTextKey + "=" + defaultTodayText);
            log.debug(holidaysKey + "=" + defaultHolidaysRaw);
            log.debug(businessIdErrorTextKey + "=" + defaultBusinessIdErrorText);
            log.debug(phoneErrorTextKey + "=" + defaultPhoneErrorText);
            log.debug(messageErrorTextKey + "=" + defaultPhoneErrorText);
        }

        LiferayDdlUtil.createDdlRecord(fields, configRecordSet, request);
        if (log.isDebugEnabled()) {
            log.debug("storeDefaultValuesToDdlRecord - DdlRecord created");
        }
    }

    private Field createField(String name, String value) {
        Field field = new Field(name, value);
        field.addValue(Locale.forLanguageTag("en-US"), value);
        return field;
    }

    private LeadConfiguration readConfiguration(DDLRecordSet configRecordSet, PortletRequest request) {
        if (log.isDebugEnabled()) {
            log.debug("readConfiguration - configRecordSet=" + configRecordSet);
        }
        LeadConfiguration configuration = new LeadConfiguration();
        if (configRecordSet == null) {
            log.warn("DDL configuration does not exists. Creating one and using default values.");

            configurationDefaultValues(configuration);
        } else {
            DDLRecord configurationRecord = null;
            try {
                List<DDLRecord> records = configRecordSet.getRecords();
                int recordsCount = records.size();
                if (recordsCount == 1) {
                    log.debug("DDL configuration record found.");
                    configurationRecord = records.get(0);
                    if (log.isDebugEnabled()) {
                        log.debug("readConfiguration - configRecordSet=" + configRecordSet);
                        log.debug("readConfiguration - configRecord=" + configurationRecord);
                    }
                } else if (recordsCount == 0) {
                    log.warn("No DDL configuration record found. Using default values.");
                    configurationDefaultValues(configuration);

                    storeDefaultValuesToDdlRecord(configRecordSet, request);
                } else {
                    log.warn("More DDL configuration record found. Using first one.");
                    configurationRecord = records.get(0);
                }

            } catch (SystemException e) {
                log.warn("Problem reading DDL configuration. Using default values.", e);
                configurationDefaultValues(configuration);
            }

            if (configurationRecord != null) {
                try {
                    Fields fields = configurationRecord.getFields();
                    configuration.setMailTo(readFieldValue(fields, mailToKey, defaultEmailAddressTo));
                    configuration.setMailFrom(readFieldValue(fields, mailFromKey, defaultEmailAddressFrom));
                    configuration.setWorkingHoursText(readFieldValue(fields, workingHoursTextKey, defaultWorkingHoursText));
                    configuration.setNextDayText(readFieldValue(fields, nextDayTextKey, defaultNextDayText));
                    configuration.setTodayText(readFieldValue(fields, todayTextKey, defaultTodayText));
                    configuration.setHolidays(parseHolidays(readFieldValue(fields, holidaysKey, defaultHolidaysRaw)));
                    configuration.setBusinessIdErrorText(readFieldValue(fields, businessIdErrorTextKey, defaultBusinessIdErrorText));
                    configuration.setPhoneErrorText(readFieldValue(fields, phoneErrorTextKey, defaultPhoneErrorText));
                    configuration.setMessageErrorText(readFieldValue(fields, messageErrorTextKey, defaultPhoneErrorText));
                } catch (PortalException e) {
                    log.warn("Problem reading DDL configuration. Using default values.", e);
                    configurationDefaultValues(configuration);
                }
            }
        }

        return configuration;
    }

    protected Set<DayOfYear> parseHolidays(String readFieldValue) {
        Set<DayOfYear> result = new HashSet<>();
        if (readFieldValue != null) {
            String[] dates = readFieldValue.split("\n");
            for (String date : dates) {
                try {
                    Matcher matcher = datePattern.matcher(date);
                    matcher.find();
                    String day = matcher.group(1);
                    String month = matcher.group(2);
                    if (!StringUtils.hasText(day) || !StringUtils.hasText(month)) {
                        continue;
                    }
                    result.add(new DayOfYear(Integer.parseInt(day), Integer.parseInt(month)));
                } catch (NumberFormatException | IllegalStateException e) {
                    log.info(e);
                    continue;
                }

            }
        }

        return result;
    }

    private String readFieldValue(Fields fields, String fieldKey, String defaultValue) {
        final String result;

        Field field = fields.get(fieldKey);
        if (field != null) {
            Serializable value = field.getValue();
            if (value != null) {
                result = value.toString();
            } else {
                log.warn("Configuration value for: " + fieldKey + " was not found. Using default value.");
                result = defaultValue;
            }
        } else {
            log.warn("Configuration field: " + fieldKey + " was not found. Using default value.");
            result = defaultValue;
        }

        return result;
    }

    private void configurationDefaultValues(LeadConfiguration configuration) {
        configuration.setMailTo(defaultEmailAddressTo);
        configuration.setMailFrom(defaultEmailAddressFrom);
    }

    /**
     * Read DDLRecordSet.
     * @return DDLRecordSet with the name or null if record was not found
     */
    private DDLRecordSet getDdlRecordSetByName(String name) {
        DDLRecordSet result = null;

        List<DDLRecordSet> ddlRecordSets;
        try {
            ddlRecordSets = DDLRecordSetLocalServiceUtil.getDDLRecordSets(0, DDLRecordSetLocalServiceUtil.getDDLRecordSetsCount());
            if (ddlRecordSets != null) {
                for (DDLRecordSet record : ddlRecordSets) {
                    if (record.getNameCurrentValue().equalsIgnoreCase(name)) {
                        result = record;
                        break;
                    }
                }
            }
        } catch (SystemException e) {
            log.warn("DDLRecordSet with name " + name + "was not found.");
        }

        return result;
    }

    private DDLRecordSet createConfigurationRecordset(PortletRequest request) {
        DDMStructure structure = LiferayDdlUtil.createDdmStructureIfNotExists("<?xml version='1.0' encoding='UTF-8'?><root available-locales=\"cs_CZ\" default-locale=\"cs_CZ\"><Name language-id=\"cs_CZ\">Lead Configuration</Name></root>", configurationDdlXsd, request);

        return LiferayDdlUtil.createDdlRecordSet(configurationRecordSetName, structure, request);
    }

    protected void setConfiguration(LeadConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public String getResponseTextByDate(LocalDateTime date) {
        log.debug("Resolving info text, date=" + date);
        final String result;

        int hour = date.getHourOfDay();
        //holiday, weekend
        if (isDayWeekendOrHoliday(date)) {
            log.debug("Resolving info text - DayWeekendOrHoliday.");
            result = configuration.getNextDayText();
        } else if ((hour > 5) && (hour < 15)) { //working hours
            log.debug("Resolving info text - working hours.");
            result = configuration.getWorkingHoursText();
        } else if ((hour >= 0) && (hour <= 5)) { //today non-working hours
            log.debug("Resolving info text - non-working hours.");
            result = configuration.getTodayText();
        } else {
            log.debug("Resolving info text - next day.");
            result = configuration.getNextDayText();
        }


        return result;
    }

    private boolean isDayWeekendOrHoliday(LocalDateTime date) {
        int dayOfWeek = date.getDayOfWeek();
        DayOfYear day = new DayOfYear(date.getDayOfMonth(), date.getMonthOfYear());

        boolean holiday = (configuration.getHolidays().contains(day) || (dayOfWeek == DateTimeConstants.SATURDAY) || (dayOfWeek == DateTimeConstants.SUNDAY));

        return holiday;
    }

    @Override
    public LocalDateTime getLatestReply(LocalDateTime date) {
        log.debug("Resolving latest reply, date=" + date);
        final LocalDateTime result;

        int hour = date.getHourOfDay();
        if (isDayWeekendOrHoliday(date)) {
            result = getNextWorkDay(date);
        } else if ((hour > 5) && (hour < 15)) { //working hours
            result = date.withDurationAdded(Duration.standardHours(3), 1);
        } else if ((hour >= 0) && (hour <= 5)) { //today non-working hours
            result = new LocalDateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(), 10, 0);
        } else {
            //result = configuration.getNextDayText();
            result = getNextWorkDay(date);
        }

        log.debug("Resolving latest reply, result=" + result);
        return result;
    }

    private LocalDateTime getNextWorkDay(LocalDateTime date) {
        LocalDateTime nextDay = date;
        do {
            nextDay = nextDay.plusDays(1);
        } while (isDayWeekendOrHoliday(nextDay));

        LocalDateTime nextDay10 = nextDay.withTime(10, 0, 0, 0);
        return nextDay10;
    }

    @Override
    public DateTimeZone getTimeZone() {
        return DateTimeZone.forID(timeZoneName);
    }
}
