package cz.artin.mvp.lead.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.validation.Valid;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.util.PortalUtil;

import cz.artin.mvp.lead.model.BusinessLeadDto;
import cz.artin.mvp.lead.service.ConfigurationService;
import cz.artin.mvp.lead.service.LeadService;

/**
 * Represents controller for Lead portlet.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@Controller
@RequestMapping(value = "VIEW")
public class LeadController {
    private static final String BINDING_RESULT_ATTRIBUTE = "org.springframework.validation.BindingResult.businessLead";

    private Log log = LogFactoryUtil.getLog(LeadController.class);

    @Autowired
    private LeadService leadService;

    @Autowired
    private ConfigurationService configurationService;

    /**
     * Default render method.
     */
    @RenderMapping
    public ModelAndView showStart(Model model, RenderRequest request, RenderResponse response) {
        configurationService.getConfiguration(request);

        final BusinessLeadDto businessLead;
        if (model.containsAttribute("businessLead")) {
            businessLead = (BusinessLeadDto) model.asMap().get("businessLead");
            BindingResult bindingResult = (BindingResult) model.asMap().get(BINDING_RESULT_ATTRIBUTE);
            if (bindingResult.hasErrors()) {
                FieldError phoneError = bindingResult.getFieldError("phone");
                if ((phoneError != null) && (bindingResult.getFieldError("phoneErrorHolder") == null)) {
                    bindingResult.rejectValue("phoneErrorHolder", null, configurationService.getConfiguration().getPhoneErrorText());
                }
                FieldError businessIdError = bindingResult.getFieldError("businessId");
                if ((businessIdError != null) && (bindingResult.getFieldError("businessIdErrorHolder") == null)) {
                    bindingResult.rejectValue("businessIdErrorHolder", null, configurationService.getConfiguration().getBusinessIdErrorText());
                }
            }
        } else {
            businessLead = new BusinessLeadDto();
        }

        ModelAndView result = new ModelAndView("start");
        result.addObject("businessLead", businessLead);
        return result;
    }

    /**
     * Display lightbox.
     */
    @RenderMapping(params = "action=finish")
    public ModelAndView showFinish(RenderRequest request, RenderResponse response) {
        log.debug("Showing lightbox");
        ModelAndView result = new ModelAndView("finish");

        result.addObject("message", configurationService.getResponseTextByDate(new LocalDateTime(configurationService.getTimeZone())));

        return result;
    }

    /**
     * Handle final status.
     */
    @ActionMapping("end")
    public void handleEnd(ActionRequest actionRequest, ActionResponse actionResponse) {
        actionResponse.setRenderParameter("action", "end");
    }

    /**
     * Display final status.
     */
    @RenderMapping(params = "action=end")
    public ModelAndView showEnd(RenderRequest request, RenderResponse response) {
        log.debug("End of webflow");
        ModelAndView result = new ModelAndView("end");

        return result;
    }


    /** Handle sending lead. */
    @ActionMapping("handleBusinessLeadForm")
    public void handleBusinessLeadForm(@ModelAttribute("businessLead") @Valid BusinessLeadDto businessLead, BindingResult bindingResult,
            ActionRequest actionRequest, ActionResponse actionResponse) {
        log.debug("Handling business lead form");

        if (!bindingResult.hasErrors()) {
            String currentCompleteUrl = PortalUtil.getCurrentURL(actionRequest);
            String currentUrl = currentCompleteUrl.substring(0, currentCompleteUrl.lastIndexOf("?"));

            leadService.createLead(businessLead, configurationService.getConfiguration(actionRequest), currentUrl);

            actionResponse.setRenderParameter("action", "finish");
        }
    }

}
