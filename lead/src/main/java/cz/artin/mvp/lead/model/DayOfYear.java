package cz.artin.mvp.lead.model;

import java.io.Serializable;

/**
 * Represents day in the year.
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class DayOfYear implements Serializable {

    private static final long serialVersionUID = 1L;

    private int dayOfMonth;

    private int month;

    /**
     * Constructor.
     * @param dayOfMonth day of month (1-31)
     * @param month given moth (1-12)
     */
    public DayOfYear(int dayOfMonth, int month) {
        super();
        this.dayOfMonth = dayOfMonth;
        this.month = month;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public int getMonth() {
        return month;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + dayOfMonth;
        result = prime * result + month;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        DayOfYear other = (DayOfYear) obj;
        if (dayOfMonth != other.dayOfMonth) {
            return false;
        }
        if (month != other.month) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DayOfYear " + dayOfMonth + ". " + month + ".";
    }
}
