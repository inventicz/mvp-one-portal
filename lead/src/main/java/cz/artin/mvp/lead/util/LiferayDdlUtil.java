package cz.artin.mvp.lead.util;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;

import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.dynamicdatalists.model.DDLRecord;
import com.liferay.portlet.dynamicdatalists.model.DDLRecordSet;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordLocalServiceUtil;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordSetLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLinkLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;

/**
 * Represents utilities for DDL manipulation.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public final class LiferayDdlUtil {
    private static final Log LOG = LogFactoryUtil.getLog(LiferayDdlUtil.class);

    private LiferayDdlUtil() {

    }

    /** Create DDM. */
    public static DDMStructure createDdmStructureIfNotExists(String name, String xsd, PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createDdmStructureIfNotExists - name=" + name + " request=" + request);
        }
        DDMStructure structure = getDdmStructureByName(name, request);
        if (structure == null) {
            structure = createDdmStructure(name, xsd, request);
        }

        return structure;
    }

    /** Read DDM structure. */
    public static DDMStructure getDdmStructureByName(final String name, PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getDdmStructureByName - name=" + name + " request=" + request);
        }
        DDMStructure result = null;
        try {
            List<DDMStructure> ddmStructures = DDMStructureLocalServiceUtil.getStructures();

            for (DDMStructure ddmStructure : ddmStructures) {
                String nameCurrentValue = ddmStructure.getNameCurrentValue();
                if (LOG.isDebugEnabled()) {
                    LOG.debug("getDdmStructureByName - nameCurrentValue=" + nameCurrentValue);
                }

                if (name.equalsIgnoreCase(nameCurrentValue)) {
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("getDdmStructureByName - found nameCurrentValue=" + nameCurrentValue);
                    }
                    result = ddmStructure;
                    break;
                }
            }
        } catch (SystemException e) {
            LOG.debug("Problem reading DDM structure", e);
        }

        return result;
    }

    /**
     * Fetch all ddm structures in system.
     *
     * @return all ddm structures in system
     */
    public static List<DDMStructure> getAllDdmStructures() {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getAllDdmStructures");
        }

        List<DDMStructure> result;

        try {
            result = DDMStructureLocalServiceUtil.getStructures();
            if (LOG.isDebugEnabled()) {
                LOG.debug("getAllDdmStructures structures=" + result);
            }
        } catch (SystemException e) {
            LOG.warn("Problem reading DDM structures.", e);
            result = Collections.<DDMStructure>emptyList();
        }

        return result;
    }

    /** Create DDMStructure for DDLRecordSet. */
    public static DDMStructure createDdmStructure(String name, String xsd, PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createDdmStructure - name=" + name + ", xsd=" + xsd + ", request=" + request);
        }

        DDMStructure ddmStructure = DDMStructureLocalServiceUtil.createDDMStructure(generateId());
        // Set names
        ddmStructure.setName(name);

        ddmStructure.setXsd(xsd);
        ddmStructure.setStorageType("xml");
        try {
            User user = PortalUtil.getUser(request);
            ddmStructure.setUserId(user.getUserId());
            ddmStructure.setCompanyId(user.getCompanyId());
            ddmStructure.setGroupId(getGroupId(request));
            ddmStructure.setClassNameId(ClassNameLocalServiceUtil.getClassNameId(DDLRecordSet.class));
            Date date = new Date();
            ddmStructure.setCreateDate(date);
            ddmStructure.setModifiedDate(date);

            DDMStructureLocalServiceUtil.addDDMStructure(ddmStructure);
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdmStructure - ddmStructure=" + ddmStructure);
            }
        } catch (SystemException | PortalException ex) {
            LOG.warn("Liferay DDL exception", ex);
            return null;
        }
        return ddmStructure;
    }

    /** Create DDL record set. */
    public static DDLRecordSet createDdlRecordSet(String name, DDMStructure structure, PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createDdlRecordSet - name=" + name + ", structure=" + structure + ", request=" + request);
        }

        DDLRecordSet result;
        try {
            DDLRecordSet recordSet = DDLRecordSetLocalServiceUtil.createDDLRecordSet(generateId());
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdlRecordSet - created recordSet=" + recordSet);
            }

            User user = PortalUtil.getUser(request);
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdlRecordSet - user found=" + user);
            }

            recordSet.setName(name);

            recordSet.setDDMStructureId(structure.getStructureId());
            recordSet.setRecordSetKey(Long.toString(generateId()));

            recordSet.setUserId(user.getUserId());
            recordSet.setCompanyId(user.getCompanyId());
            recordSet.setGroupId(getGroupId(request));
            Date date = new Date();
            recordSet.setCreateDate(date);
            recordSet.setModifiedDate(date);

            result = DDLRecordSetLocalServiceUtil.addDDLRecordSet(recordSet);
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdlRecordSet - addDDLRecordSet " + result);
            }

            // Dynamic data mapping structure link
            long classNameId = PortalUtil.getClassNameId(DDLRecordSet.class);
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdlRecordSet - classNameId=" + classNameId);
            }

            ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
            if (LOG.isDebugEnabled()) {
                LOG.debug("createDdlRecordSet - serviceContext=" + serviceContext);
            }

            DDMStructureLinkLocalServiceUtil.addStructureLink(classNameId, result.getRecordSetId(), result.getDDMStructureId(), serviceContext);
        } catch (PortalException | SystemException ex) {
            LOG.warn("Liferay DDLRecordSet exception", ex);
            result = null;
        }
        return result;
    }

    /** Create DDL record. */
    public static DDLRecord createDdlRecord(Fields fields, DDLRecordSet recordSet, PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("createDdlRecord - fields=" + fields + ", recordSet=" + recordSet + ", request=" + request);
        }

        DDLRecord result;
        try {
            ServiceContext serviceContext = ServiceContextFactory.getInstance(request);
            result = DDLRecordLocalServiceUtil.addRecord(PortalUtil.getUser(request).getUserId(), getGroupId(request), recordSet.getRecordSetId(), 0,
                    fields, serviceContext);
        } catch (PortalException | SystemException ex) {
            LOG.debug("Liferay DDLRecord exception", ex);
            result = null;
        }
        return result;
    }



    private static long generateId() {
        long result;
        try {
            result =  CounterLocalServiceUtil.increment();
        } catch (SystemException ex) {
            LOG.warn("Liferay Counter service exception", ex);
            result = System.currentTimeMillis();
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("generateId - " + result);
        }

        return result;
    }

    private static long getGroupId(PortletRequest request) {
        if (LOG.isDebugEnabled()) {
            LOG.debug("getGroupId - " + request);
        }
        ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
        if (LOG.isDebugEnabled()) {
            LOG.debug("getGroupId - themeDisplay " + themeDisplay);
        }

        long groupId = themeDisplay.getLayout().getGroupId();
        if (LOG.isDebugEnabled()) {
            LOG.debug("getGroupId - goupId found " + groupId);
        }
        return groupId;
    }
}
