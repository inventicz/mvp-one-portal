package cz.artin.mvp.lead.model.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Represents validator for CZ phone number.
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        final boolean result;
        if (value != null) {
            String phoneNumber = value.replaceAll("\\s", "");
            result = phoneNumber.matches("[0-9]{9}");
        } else {
            result = false;
        }

        return result;
    }
}
