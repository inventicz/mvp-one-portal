package cz.artin.mvp.lead.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Set;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.lead.AbstractTest;
import cz.artin.mvp.lead.model.DayOfYear;

/**
 * Represents tests for parse date utility method.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class ConfigurationServiceParseHolidaysTest extends AbstractTest {

    @Autowired
    private ConfigurationServiceImpl service;

    @Test
    public void parse() {
        String holidays = "1.1.\n1.2.";

        Set<DayOfYear> parsedHolidays = service.parseHolidays(holidays);

        assertEquals(2, parsedHolidays.size());
        assertTrue(parsedHolidays.contains(new DayOfYear(1, 1)));
        assertTrue(parsedHolidays.contains(new DayOfYear(1, 2)));
    }

    @Test
    public void parseInvalid() {
        String holidays = "1.1.\nxkkdk\n32.12\n1.2.";

        Set<DayOfYear> parsedHolidays = service.parseHolidays(holidays);

        assertEquals(2, parsedHolidays.size());
        assertTrue(parsedHolidays.contains(new DayOfYear(1, 1)));
        assertTrue(parsedHolidays.contains(new DayOfYear(1, 2)));
    }
}
