package cz.artin.mvp.lead;

import org.junit.runner.RunWith;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * Configure certain test classes for spring and transactions.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = {"classpath:test-spring.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public abstract class AbstractTest {


}
