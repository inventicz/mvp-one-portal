package cz.artin.mvp.lead.service;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.lead.AbstractTest;
import cz.artin.mvp.lead.model.DayOfYear;
import cz.artin.mvp.lead.model.LeadConfiguration;

/**
 * Represents tests for message shown to user.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class ConfigurationServiceResponseTextTest extends AbstractTest {

    @Autowired
    private ConfigurationServiceImpl service;

    private LeadConfiguration config;

    @Before
    public void setup() {
        config = new LeadConfiguration();
        config.setNextDayText("NEXT_DAY_TEST");
        config.setTodayText("TODAY_TEST");
        config.setWorkingHoursText("WORKING_HOURS_TEST");
        Set<DayOfYear> holidays = new HashSet<>();
        holidays.add(new DayOfYear(1, 1));
        holidays.add(new DayOfYear(1,5));
        holidays.add(new DayOfYear(8,5));
        holidays.add(new DayOfYear(5,7));
        config.setHolidays(holidays);

        service.setConfiguration(config);
    }

    @Test
    public void day0101() {
        LocalDateTime date = new LocalDateTime(2000, 1, 1, 0, 0);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void saturday() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 26, 0, 0);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void sunday() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 27, 0, 0);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayWorkingHours1() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 6, 0);
        assertEquals("WORKING_HOURS_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayWorkingHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 15, 0);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayWorkingHours3() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 14, 59);
        assertEquals("WORKING_HOURS_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayMorningHours() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 0, 0);
        assertEquals("TODAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayMorningHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 5, 59);
        assertEquals("TODAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void tuesdayMorningHours() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 29, 0, 0);
        assertEquals("TODAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void fridayMorningHours() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 29, 0, 0);
        assertEquals("TODAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayAfternoonHours() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 15, 1);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayAfternoonHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 16, 0);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

    @Test
    public void mondayAfternoonHours3() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 23, 59);
        assertEquals("NEXT_DAY_TEST", service.getResponseTextByDate(date));
    }

}
