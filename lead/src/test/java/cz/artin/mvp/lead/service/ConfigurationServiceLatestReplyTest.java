package cz.artin.mvp.lead.service;

import static org.junit.Assert.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDateTime;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import cz.artin.mvp.lead.AbstractTest;
import cz.artin.mvp.lead.model.DayOfYear;
import cz.artin.mvp.lead.model.LeadConfiguration;

/**
 * Represents tests for latest reply time.
 *
 * @author Petr Stahl [petr.stahl@artin.cz]
 *
 */
public class ConfigurationServiceLatestReplyTest extends AbstractTest {

    @Autowired
    private ConfigurationServiceImpl service;

    private LeadConfiguration config;

    @Before
    public void setup() {
        config = new LeadConfiguration();
        config.setNextDayText("NEXT_DAY_TEST");
        config.setTodayText("TODAY_TEST");
        config.setWorkingHoursText("WORKING_HOURS_TEST");
        Set<DayOfYear> holidays = new HashSet<>();
        holidays.add(new DayOfYear(1, 1));
        holidays.add(new DayOfYear(1, 5));
        holidays.add(new DayOfYear(8, 5));
        holidays.add(new DayOfYear(5, 7));
        holidays.add(new DayOfYear(6, 7));
        config.setHolidays(holidays);

        service.setConfiguration(config);
    }

    @Test
    public void day0101() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.JANUARY, 1, 0, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.JANUARY, 2, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void saturday() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.FEBRUARY, 7, 0, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.FEBRUARY, 9, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void saturdayBeforeHoliday() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.JULY, 4, 0, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.JULY, 7, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void sunday() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.FEBRUARY, 8, 0, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.FEBRUARY, 9, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayWorkingHours1() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 6, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 9, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayWorkingHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 15, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 22, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayWorkingHours3() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 14, 59);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 17, 59);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayMorningHours() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 0, 0);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayMorningHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 5, 59);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayAfternoonHours2() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 16, 00);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 22, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

    @Test
    public void mondayAfternoonHours3() {
        LocalDateTime date = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 21, 23, 59);

        LocalDateTime desiredDate = new LocalDateTime(2015, DateTimeConstants.SEPTEMBER, 22, 10, 0);
        assertEquals(desiredDate, service.getLatestReply(date));
    }

}
